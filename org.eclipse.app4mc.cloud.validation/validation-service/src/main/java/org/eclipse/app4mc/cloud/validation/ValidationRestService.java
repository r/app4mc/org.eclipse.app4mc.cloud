/*********************************************************************************
 * Copyright (c) 2020, 2021 Robert Bosch GmbH and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */
package org.eclipse.app4mc.cloud.validation;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.CacheControl;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Link;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.PathSegment;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.core.UriBuilder;
import javax.ws.rs.core.UriInfo;

import org.eclipse.app4mc.amalthea.model.Amalthea;
import org.eclipse.app4mc.amalthea.model.io.AmaltheaFileHelper;
import org.eclipse.app4mc.amalthea.model.io.AmaltheaLoader;
import org.eclipse.app4mc.amalthea.model.util.ModelUtil;
import org.eclipse.app4mc.validation.core.IProfile;
import org.eclipse.app4mc.validation.util.ProfileManager;
import org.eclipse.app4mc.validation.util.ValidationExecutor;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.jaxrs.whiteboard.propertytypes.JSONRequired;
import org.osgi.service.jaxrs.whiteboard.propertytypes.JaxrsResource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

@Component(service=ValidationRestService.class)
@JaxrsResource
@Produces(MediaType.APPLICATION_JSON)
@JSONRequired
@Path("app4mc/validation")
public class ValidationRestService {

	private static final String TEMP_DIR_PREFIX = "app4mc_validation_";

	private static final Logger LOGGER = LoggerFactory.getLogger(ValidationRestService.class);
	
	private static final String PROGRESS_MARKER = "in_progress";
	private static final String ERROR_MARKER = "error";
	private static final String FINISHED_MARKER = "finished";
	
	private static final String ERROR_FILE = "error.txt";

	private static final String PASSED_MARKER = "passed";
	private static final String FAILED_MARKER = "failed";

	private final String defaultBaseDir = System.getProperty("java.io.tmpdir");
	
	private ExecutorService executor = Executors.newFixedThreadPool(1);
	
    @Reference
    private ProfileManager manager;
	
	@GET
	@Path("config")
	public String config() {
		ObjectMapper mapper = new ObjectMapper();
		
		ObjectNode config = mapper.createObjectNode();
		config.put("description", "Validate the input model file.");
		
		ObjectNode input = mapper.createObjectNode();
		input.put("type", "amxmi");
		input.put("version", ModelUtil.MODEL_VERSION);
		input.put("archive-supported", true);
		config.set("input", input);

		// the validation produces no output that is consumed by a following service

		ObjectNode parameter = mapper.createObjectNode();
		ObjectNode profiles = mapper.createObjectNode();
		profiles.put("name", "Validation Profiles");
		profiles.put("description", "The validation profiles that should be executed.\nIf nothing is selected the Amalthea Standard Validations profile is executed.");
		profiles.put("cardinality", "multiple");
		// the default value to use is the Amalthea Standard Validations profile
		profiles.put("value", "Amalthea Standard Validations");
		ArrayNode values = profiles.putArray("values");
		
    	List<String> availableProfiles = manager.getRegisteredValidationProfiles().values().stream()
			.sorted((p1, p2) -> p1.getName().compareTo(p2.getName()))
			.map(profile -> profile.getName())
			.collect(Collectors.toList());
    	availableProfiles.forEach(v -> values.add(v));
		
		parameter.set("profiles", profiles);
		config.set("parameter", parameter);
	
		try {
			return mapper.writerWithDefaultPrettyPrinter().writeValueAsString(config);
		} catch (JsonProcessingException e) {
			return "Error in generating configuration definition: " + e.getMessage();
		}
	}
	
	@GET
	@Path("profiles")
	public String profiles() {
		ObjectMapper objectMapper = new ObjectMapper();
        try {
        	List<String> availableProfiles = manager.getRegisteredValidationProfiles().values().stream()
    			.sorted((p1, p2) -> p1.getName().compareTo(p2.getName()))
    			.map(profile -> profile.getName())
    			.collect(Collectors.toList());
			return objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(availableProfiles);
        } catch (JsonProcessingException e) {
        	return "Error in generating profiles response: " + e.getMessage();
        }
	}

    @POST
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    public Response upload(@Context HttpServletRequest request, @Context UriInfo uriInfo, @Context ServletContext context) throws IOException, ServletException {
    	
    	Link self = Link.fromUriBuilder(uriInfo.getAbsolutePathBuilder()).rel("self").build();

    	Part part = request.getPart("file");
    	if (part != null && part.getSubmittedFileName() != null && part.getSubmittedFileName().length() > 0) {
    		String filename = part.getSubmittedFileName();
    		try (InputStream is = part.getInputStream()) {
    			java.nio.file.Path tempFolderPath = Files.createTempDirectory(TEMP_DIR_PREFIX);
    			
    			// extract uuid from pathname
    			String uuid = tempFolderPath.toString().substring(tempFolderPath.toString().lastIndexOf('_') + 1);
    			
    			// copy file to temporary location
    			java.nio.file.Path uploaded = Paths.get(tempFolderPath.toString(), filename);
    			Files.copy(is, uploaded);
    			
    			if (Files.exists(uploaded)) {
    				// mark uuid in progress
    				Map<String, String> registry = getRegistry(context);
    				registry.put(uuid, PROGRESS_MARKER);
    				
    				List<String> selectedProfiles = getSelectedProfiles(request);
    				
    				// trigger asynchronous processing
    				executor.execute(() -> {
    					
    					try {
    						File uploadedFile = uploaded.toFile();
    						
    						// verify the uploaded model version
    						if (!AmaltheaFileHelper.isModelVersionValid(uploadedFile)) {
        		    			registry.put(uuid, ERROR_MARKER);
        		    			error(tempFolderPath, 
        		    					"Error: Invalid model version " + AmaltheaFileHelper.getModelVersion(uploadedFile) + ". Expected model version: " + ModelUtil.MODEL_VERSION, null);
        		    			return;
    						}
    						
        		    		// load uploaded model file
        		    		Amalthea model = AmaltheaLoader.loadFromFile(uploaded.toFile());
        		    		
        		    		if (model == null) {
        		    			registry.put(uuid, ERROR_MARKER);
        		    			error(tempFolderPath, "Error: No model loaded!", null);
        		    			return;
        		    		}
        		    		
        		    		// get selected profiles from profile manager
        		    		List<Class<? extends IProfile>> profileList = manager.getRegisteredValidationProfiles().values().stream()
        		    			.filter(profile -> selectedProfiles.contains(profile.getName()))
        		    			.map(profile -> profile.getProfileClass())
        		    			.collect(Collectors.toList());
        		    		
        		    		ValidationExecutor executor = new ValidationExecutor(profileList);
        		    		executor.validate(model);
        		    		
        		    		// dump validation result to file
        		    		java.nio.file.Path resultFile = Files.createFile(Paths.get(tempFolderPath.toString(), "validation-results.txt"));
        		    		try (PrintStream print = new PrintStream(new FileOutputStream(resultFile.toFile()))) {
        		    			executor.dumpResultMap(print);
        		    		}

        		    		boolean failed = executor.getResults().stream()
        		    				.anyMatch(diag -> diag.getSeverity() == org.eclipse.emf.common.util.Diagnostic.ERROR);
        		    		if (!failed) {
        		    			Files.createFile(Paths.get(tempFolderPath.toString(), PASSED_MARKER));
        		    		} else {
        		    			Files.createFile(Paths.get(tempFolderPath.toString(), FAILED_MARKER));
        		    		}
    					} catch (IOException e) {
    						LOGGER.error("Failed to write validation results", e);
    		    			registry.put(uuid, ERROR_MARKER);
    						error(tempFolderPath, "Failed to write validation results", e);
						} finally {
    						if (!ERROR_MARKER.equals(getRegistry(context).get(uuid))) {
    							getRegistry(context).put(uuid, FINISHED_MARKER);
    						}
    					}
    				});
    				
    				// file upload succeeded and processing is triggered
    				Link statusLink = Link.fromUriBuilder(uriInfo.getAbsolutePathBuilder()
                            .path(uuid))
    						.rel("status")
    						.build();
    				
    				return Response
    						.created(statusLink.getUri())
    						.entity(uuid)
    						.links(self, statusLink)
    						.build();
    			} else {
    				// file upload failed
    				return Response
    						.status(Status.NOT_FOUND)
    						.entity("Model file upload failed!")
    						.links(self)
    						.build();
    			}
    		}
    	}

    	return Response
    			.status(Status.BAD_REQUEST)
    			.entity("No model file provided!")
    			.links(self)
    			.build();
    }
    
    private List<String> getSelectedProfiles(HttpServletRequest request) throws IOException, ServletException {

    	// first check if the profiles are sent as query parameter
    	String[] profiles = request.getParameterValues("profiles");
    	if (profiles != null) {
    		return Arrays.asList(profiles);
    	} else {
    		// check if the profiles are sent as post parameter in the multipart request
    		List<String> collected = new ArrayList<>();
			for (Part supportPart : request.getParts()) {
				if (supportPart.getName().equals("profiles")) {
					try (BufferedReader reader = new BufferedReader(new InputStreamReader(supportPart.getInputStream()))) {
						List<String> collect = reader.lines().collect(Collectors.toList());
						if (collect != null && !collect.isEmpty()) {
							collected.addAll(collect);
						}
					}
				}
			}
    		if (!collected.isEmpty()) {
    			return collected;
    		}
    	}
    	
    	// neither query parameter nor multipart post parameter found, return default
    	return Arrays.asList("Amalthea Standard Validations");
    }
    
    @Path("{uuid}")
    @GET
    public Response status(@PathParam("uuid") String uuid, @Context UriInfo uriInfo, @Context ServletContext context) throws IOException {
    	CacheControl cacheControl = new CacheControl();
    	cacheControl.setNoCache(true);
    	cacheControl.setNoStore(true);
    	
    	Link self = Link.fromUriBuilder(uriInfo.getAbsolutePathBuilder()).rel("self").build();
    	
    	java.nio.file.Path tempFolderPath = Paths.get(defaultBaseDir, TEMP_DIR_PREFIX + uuid);
    	if (!Files.exists(tempFolderPath)) {
			return Response
					.status(Status.NOT_FOUND)
					.entity("No status resource available for id " + uuid)
					.build();
    	}
    	
    	String status = getRegistry(context).get(uuid);
    	
    	boolean hasErrorFile = false;
    	try (Stream<java.nio.file.Path> files = Files.list(tempFolderPath)) {
    		hasErrorFile = files.anyMatch(path -> path.endsWith(ERROR_FILE));
    	}
    	
    	if (PROGRESS_MARKER.equals(status)) {
    		return Response
    				.accepted()
    				.links(self)
    				.cacheControl(cacheControl)
    				.build();
    	} else if (ERROR_MARKER.equals(status) || hasErrorFile) {
        	// processing finished with error
    		Link errorLink = Link.fromUriBuilder(uriInfo.getAbsolutePathBuilder()
                    .path("error"))
    				.rel("error")
    				.build();

    		return Response
    				.noContent()
    				.links(self, errorLink)
    				.cacheControl(cacheControl)
    				.build();
    	}
    	
    	// processing is finished
		Link downloadLink = Link.fromUriBuilder(uriInfo.getAbsolutePathBuilder()
                .path("download"))
				.rel("result")
				.build();

		return Response
				.ok()
				.links(self, downloadLink)
				.cacheControl(cacheControl)
				.build();
    }

    @Path("{uuid}/download")
    @GET
    @Produces({ MediaType.APPLICATION_OCTET_STREAM, MediaType.APPLICATION_JSON } )
    public Response download(@PathParam("uuid") String uuid, @Context UriInfo uriInfo, @Context ServletContext context) throws IOException {
    	Link self = Link.fromUriBuilder(uriInfo.getAbsolutePathBuilder()).rel("self").build();

    	java.nio.file.Path tempFolderPath = Paths.get(defaultBaseDir, TEMP_DIR_PREFIX + uuid);
    	if (!Files.exists(tempFolderPath)) {
			return Response
					.status(Status.NOT_FOUND)
					.entity("No download resource available for id " + uuid)
					.build();
    	}

    	// if process is in progress, the download resource is 404
    	String status = getRegistry(context).get(uuid);
    	
    	if (PROGRESS_MARKER.equals(status)) {
    		return Response
    				.status(Status.NOT_FOUND)
    				.entity("Process is still in progresss")
    				.links(self)
    				.build();
    	}

    	java.nio.file.Path path = Paths.get(tempFolderPath.toString(), "validation-results.txt");
    	if (!Files.exists(path)) {
			return Response
					.status(Status.NOT_FOUND)
					.entity("No validation result available!")
    				.links(self)
					.build();
		}
    	
    	// set status based on validation result
    	boolean hasFailedMarker = false;
    	try (Stream<java.nio.file.Path> files = Files.list(tempFolderPath)) {
    		hasFailedMarker = files.anyMatch(p -> p.endsWith(FAILED_MARKER));
    	}
    	
    	int responseStatus = HttpServletResponse.SC_OK;
    	if (hasFailedMarker) {
    		responseStatus = HttpServletResponse.SC_BAD_REQUEST;
    	}

		List<PathSegment> pathSegments = uriInfo.getPathSegments();
    	UriBuilder uriBuilder = uriInfo.getAbsolutePathBuilder().replacePath("");
    	for (int i = 0; i < pathSegments.size() - 1; i++) {
    		uriBuilder.path(pathSegments.get(i).getPath());
    	}
		Link deleteLink = Link.fromUriBuilder(uriBuilder)
				.rel("delete")
				.build();

		return Response
				.status(responseStatus)
				.entity(path.toFile())
				.header("Content-Disposition", "attachment; filename=\"" + path.toFile().getName() + "\"")
	    		// set special header to tell that the result should not be used in the next process step
				.header("x-app4mc-use-result", "false")
				.links(self, deleteLink)
				.build();
    }
    
    @Path("{uuid}/error")
    @GET
    @Produces({ MediaType.APPLICATION_OCTET_STREAM, MediaType.APPLICATION_JSON } )
    public Response error(@PathParam("uuid") String uuid, @Context UriInfo uriInfo) throws IOException {
    	Link self = Link.fromUriBuilder(uriInfo.getAbsolutePathBuilder()).rel("self").build();

    	java.nio.file.Path tempFolderPath = Paths.get(defaultBaseDir, TEMP_DIR_PREFIX + uuid);
    	
    	if (!Files.exists(tempFolderPath)) {
    		return Response.status(Status.NOT_FOUND).build();
    	}
    	
    	boolean hasErrorFile = false;
    	try (Stream<java.nio.file.Path> files = Files.list(tempFolderPath)) {
    		hasErrorFile = files.anyMatch(path -> path.endsWith(ERROR_FILE));
    	}

    	// if there is no error file, the error resource is 404
    	if (!hasErrorFile) {
    		return Response
    				.status(Status.NOT_FOUND)
    				.entity("No error occured")
    				.links(self)
    				.build();
    	}
    	
    	java.nio.file.Path errorFilePath = Paths.get(tempFolderPath.toString(), ERROR_FILE);
    	
		List<PathSegment> pathSegments = uriInfo.getPathSegments();
    	UriBuilder uriBuilder = uriInfo.getAbsolutePathBuilder().replacePath("");
    	for (int i = 0; i < pathSegments.size() - 1; i++) {
    		uriBuilder.path(pathSegments.get(i).getPath());
    	}
		Link deleteLink = Link.fromUriBuilder(uriBuilder)
				.rel("delete")
				.build();

		return Response
				.ok(errorFilePath.toFile())
				.header("Content-Disposition", "attachment; filename=\"" + errorFilePath.toFile().getName() + "\"")
				.links(self, deleteLink)
				.build();
    }
    
    @Path("{uuid}")
    @DELETE
    public Response delete(@PathParam("uuid") String uuid, @Context ServletContext context) throws IOException {
    	java.nio.file.Path path = Paths.get(defaultBaseDir, TEMP_DIR_PREFIX + uuid);
    	
    	if (!Files.exists(path)) {
    		return Response.status(Status.NOT_FOUND).build();
    	}
    	
    	Files.walk(path)
	    	.sorted(Comparator.reverseOrder())
	    	.map(java.nio.file.Path::toFile)
	    	.forEach(File::delete);
    	
    	getRegistry(context).remove(uuid);
    	
    	return Response.ok().build();
    }


	private void error(java.nio.file.Path resultFolder, String message, Exception exception) {
		try {
			java.nio.file.Path errorFilePath = Files.createFile(Paths.get(resultFolder.toString(), ERROR_FILE));
			try (PrintWriter writer = new PrintWriter(Files.newOutputStream(errorFilePath))) {
				writer.append(message).append(System.lineSeparator());
				if (exception != null) {
					exception.printStackTrace(writer);
				}
			}
			
		} catch (IOException e) {
			LOGGER.error("Failed to write error.txt", e);
		}
	}
	
	@SuppressWarnings("unchecked")
	private static synchronized Map<String, String> getRegistry(ServletContext context) {
			if (context.getAttribute("_REGISTRY") == null) {
				context.setAttribute("_REGISTRY", new ConcurrentHashMap<String, String>());
			}
			return (Map<String, String>) context.getAttribute("_REGISTRY");
	}
}
