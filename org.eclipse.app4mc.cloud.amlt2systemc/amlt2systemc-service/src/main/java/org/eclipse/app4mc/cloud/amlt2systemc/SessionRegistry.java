/*********************************************************************************
 * Copyright (c) 2021 Robert Bosch GmbH and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */
package org.eclipse.app4mc.cloud.amlt2systemc;

import java.util.concurrent.ConcurrentHashMap;

import org.osgi.service.component.annotations.Component;

/**
 * The session registry that stores the status of the available sessions.
 */
@Component(service = SessionRegistry.class)
public class SessionRegistry {

	public enum Status {
		PROGRESS, ERROR, FINISHED
	}
	
	private ConcurrentHashMap<String, Status> registry = new ConcurrentHashMap<>();
	
	public Status getStatus(String uuid) {
		return this.registry.get(uuid);
	}
	
	public void setStatus(String uuid, Status status) {
		this.registry.put(uuid, status);
	}
	
	public void removeStatus(String uuid) {
		this.registry.remove(uuid);
	}
	
	public void setInProgress(String uuid) {
		setStatus(uuid, Status.PROGRESS);
	}
	
	public void setError(String uuid) {
		setStatus(uuid, Status.ERROR);
	}
	
	public void setFinished(String uuid) {
		setStatus(uuid, Status.FINISHED);
	}
	
	public boolean isInProgress(String uuid) {
		return Status.PROGRESS.equals(getStatus(uuid));
	}
	
	public boolean isFinished(String uuid) {
		return Status.FINISHED.equals(getStatus(uuid));
	}
	
	public boolean isError(String uuid) {
		return Status.ERROR.equals(getStatus(uuid));
	}
	
}
