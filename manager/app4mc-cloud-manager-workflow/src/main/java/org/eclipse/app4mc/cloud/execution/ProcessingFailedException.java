/*********************************************************************************
 * Copyright (c) 2020 Robert Bosch GmbH and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */
package org.eclipse.app4mc.cloud.execution;

/**
 * Exception that is thrown when an error occurs in the workflow processing, e.g. validation failed.
 */
public class ProcessingFailedException extends RuntimeException {

	private static final long serialVersionUID = -8050430473958650092L;

	public ProcessingFailedException(String message) {
		super(message);
	}

	public ProcessingFailedException(String message, Throwable cause) {
		super(message, cause);
	}

}
