/*********************************************************************************
 * Copyright (c) 2020, 2021 Robert Bosch GmbH and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 *     Dortmund University of Applied Sciences and Arts - Bug 570871
 ********************************************************************************
 */
package org.eclipse.app4mc.cloud.execution;

import java.nio.file.Path;

import org.eclipse.app4mc.cloud.execution.ProcessLog.Action;
import org.eclipse.app4mc.cloud.storage.StorageService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CloudServiceExecutionRunnable implements Runnable {

	private final StorageService storageService;
	private final MessageDispatcher messagingTemplate;

	private String uuid;
	private String originalFilename;
	private WorkflowStatus workflowStatus;

	private final Logger logger = LoggerFactory.getLogger(this.getClass());
		
	public CloudServiceExecutionRunnable(
			StorageService storageService, 
			MessageDispatcher messagingTemplate, 
			String uuid, 
			String originalFilename, 
			WorkflowStatus workflowStatus) {
		
		this.storageService = storageService;
		this.messagingTemplate = messagingTemplate;
		
		this.uuid = uuid;
		this.originalFilename = originalFilename;
		this.workflowStatus = workflowStatus;
	}

	@Override
	public void run() {
		try {
			Path inputFile = this.storageService.load(this.uuid, this.originalFilename);
			
			ServiceNodeProcessingTask rootTask = 
					new ServiceNodeProcessingTask(
							this.workflowStatus.getServiceRootNode(),
							inputFile,
							this.storageService,
							this.messagingTemplate,
							this.uuid,
							this.workflowStatus);

			logger.info("started root task");
			rootTask.fork();
			logger.info("waiting for root task to finish");
			// join the task to indicate the entire workflow is done
			rootTask.join();
			logger.info("finished root task");
		} finally {
			this.workflowStatus.done();
			WorkflowStatusHelper.saveWorkflowStatus(
					this.workflowStatus, 
					this.storageService.load(this.uuid, "workflowstatus.json").toFile());
			
			if (this.messagingTemplate != null) {
				// ensure that the done message is sent in any way to ensure the ui gets the final update
				// needed in case the process finishes while the page reloads
				long start = System.currentTimeMillis();
				long end = System.currentTimeMillis();
				
				long timeout = 10_000l;
				
				while (!this.workflowStatus.isConnected()) {
					try {
						Thread.sleep(1000);
					} catch (InterruptedException e) {
						// Restore interrupted state...
						Thread.currentThread().interrupt();
					}
					
					end = System.currentTimeMillis();
					
					// don't wait longer than 10 seconds to get connected
					if (timeout > 0 && (end - start) > timeout) {
						break;
					}
				}
				
				this.messagingTemplate.send(
						"/topic/process-updates/" + this.uuid, 
						new ProcessLog(Action.DONE, null, this.uuid));
			}
		}
	}
}