/*********************************************************************************
 * Copyright (c) 2020 Robert Bosch GmbH and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */
package org.eclipse.app4mc.cloud.execution;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import kong.unirest.HttpResponse;

/**
 * Helper class to get information out of response headers.
 */
public final class HeaderHelper {

	private HeaderHelper() {
		// private constructor for helper class
	}
	
	private static final String LINK_DELIMITER = ",";
	private static final String LINK_PARAM_DELIMITER = ";";

	/**
	 * Extract the Link headers from the {@link HttpResponse}. Used to normalize
	 * different ways of sending the Link header. Typically every Link is sent as
	 * its own header. But it is also supported to send all Link header in one
	 * header element.
	 * 
	 * @param statusResponse The {@link HttpResponse} from which the Link headers
	 *                       should be extracted.
	 * @return The Link entries in the response header.
	 */
	public static List<String> getLinkHeaders(HttpResponse<?> statusResponse) {
		ArrayList<String> result = new ArrayList<>();
		
		List<String> linkHeaders = statusResponse.getHeaders().get("Link");
		for (String linkHeader : linkHeaders) {
			String[] links = linkHeader.split(LINK_DELIMITER);
			
			for (String link : links) {
				result.add(link.trim());
			}
		}
		
		return result;
	}
	
	/**
	 * Extracts the url from the link header and returns the url that has the
	 * specified rel attribute set.
	 * 
	 * @param linkHeaders The link headers to parse.
	 * @param rel         The value for the rel param for which the url is
	 *                    requested.
	 * @param baseUrl     The base URL of the service.
	 * @return The url for the specified rel param or <code>null</code> if there is
	 *         no link for the given rel.
	 */
	public static String getUrlFromLink(List<String> linkHeaders, String rel, String baseUrl) {
		
		for (String linkHeader : linkHeaders) {
			String[] links = linkHeader.split(LINK_DELIMITER);
			
			for (String link : links) {
				String[] segments = link.split(LINK_PARAM_DELIMITER);
				
				if (segments.length < 2) {
					continue;
				}
				
				String url = segments[0].trim();
				if (!url.startsWith("<") || !url.endsWith(">")) {
					continue;
				}
				url = url.substring(1, url.length() - 1);
				
				for (int i = 1; i < segments.length; i++) {
					String[] param = segments[i].trim().split("=");
					if (param.length < 2 || !"rel".equals(param[0])) {
						continue;
					}
					
					String relValue = param[1];
					if (relValue.startsWith("\"") && relValue.endsWith("\"")) {
						relValue = relValue.substring(1, relValue.length() - 1);
					}
					
					if (rel.equals(relValue)) {
						// SECURITY: ensure that host in link matches host in configured service
						if (!isValid(url, baseUrl)) {
							throw new ProcessingFailedException(
									"The link for rel '" + rel + "' does not match with the service base URL '" + baseUrl + "': " + url);
						}
						
						return url;
					}
				}
			}
		}
		
		return null;
	}
	
	/**
	 * Extracts the url from the link header and returns the url that has the
	 * specified rel attribute set.
	 * 
	 * @param linkHeaders The link headers to parse.
	 * @param rel         The value for the rel param for which the url is
	 *                    requested.
	 * @param baseUrl     The base URL of the service.
	 * @return The url for the specified rel param or <code>null</code> if there is
	 *         no link for the given rel.
	 */
	public static List<String> getUrlsFromLink(List<String> linkHeaders, String rel, String baseUrl) {
		
		ArrayList<String> results = new ArrayList<>();
		ArrayList<String> errors = new ArrayList<>();
		
		for (String linkHeader : linkHeaders) {
			String[] links = linkHeader.split(LINK_DELIMITER);
			
			for (String link : links) {
				String[] segments = link.split(LINK_PARAM_DELIMITER);
				
				if (segments.length < 2) {
					continue;
				}
				
				String url = segments[0].trim();
				if (!url.startsWith("<") || !url.endsWith(">")) {
					continue;
				}
				url = url.substring(1, url.length() - 1);
				
				for (int i = 1; i < segments.length; i++) {
					String[] param = segments[i].trim().split("=");
					if (param.length < 2 || !"rel".equals(param[0])) {
						continue;
					}
					
					String relValue = param[1];
					if (relValue.startsWith("\"") && relValue.endsWith("\"")) {
						relValue = relValue.substring(1, relValue.length() - 1);
					}
					
					if (rel.equals(relValue)) {
						// SECURITY: ensure that host in link matches host in configured service
						if (isValid(url, baseUrl)) {
							results.add(url);
						} else {
							errors.add("The link for rel '" + rel + "' does not match with the service base URL '" + baseUrl + "': " + url);
						}
					}
				}
			}
		}
		
		if (results.isEmpty() && !errors.isEmpty()) {
			String error = String.join("\n", errors);
			throw new ProcessingFailedException(error);
		}
		
		return results;
	}
	
	/**
	 * Extracts the filename from the Content-Disposition header.
	 * 
	 * @param header The Content-Disposition header to parse.
	 * @return The filename from the header or <code>null</code> if no filename
	 *         could be extracted.
	 */
	public static String getFilenameFromHeader(String header) {
		String[] segments = header.split(LINK_PARAM_DELIMITER);
		for (String segment : segments) {
			if (segment.trim().startsWith("filename=")) {
				String filename = segment.substring(segment.indexOf('=') + 1);
				if (filename.startsWith("\"") && filename.endsWith("\"")) {
					filename = filename.substring(1, filename.length() - 1);
				}
				return filename;
			}
		}
		return null;
	}

	/**
	 * Verify if the URL to check has the same host as the given base URL. This
	 * method is used to avoid that a service result redirects to another server.
	 * 
	 * @param toCheck The URL String that should be checked.
	 * @param base    The base URL String to check against.
	 * @return <code>true</code> if the host parts of the URLs are equal,
	 *         <code>false</code> in any other case.
	 */
	public static boolean isValid(String toCheck, String base) {
		if ((toCheck == null || "".equals(toCheck)) || (base == null || "".equals(base))) {
			throw new ProcessingFailedException("URLs to compare can not be empty");
		}
		
		URL toCheckUrl = null; 
		try {
			toCheckUrl = new URL(toCheck);
		} catch (MalformedURLException e) {
			throw new ProcessingFailedException("URL to check is not in a valid format", e);
		}

		URL baseUrl = null; 
		try {
			baseUrl = new URL(base);
		} catch (MalformedURLException e) {
			throw new ProcessingFailedException("URL to check against is not in a valid format", e);
		}
		
		if (baseUrl != null && toCheckUrl != null) {
			// for now we only check that host:port are equal
			return baseUrl.getAuthority().equals(toCheckUrl.getAuthority());
//			return toCheckUrl.getAuthority().equals(baseUrl.getAuthority()) && toCheckUrl.getPath().startsWith(baseUrl.getPath());
		}
		
		return false;
	}

}
