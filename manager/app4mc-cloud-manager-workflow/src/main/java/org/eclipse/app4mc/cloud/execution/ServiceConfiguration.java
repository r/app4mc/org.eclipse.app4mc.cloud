/*********************************************************************************
 * Copyright (c) 2020, 2021 Robert Bosch GmbH and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 *     Dortmund University of Applied Sciences and Arts
 ********************************************************************************
 */
package org.eclipse.app4mc.cloud.execution;

import java.util.ArrayList;
import java.util.List;

public class ServiceConfiguration {

	/**
	 * User friendly name of the service for which this configuration is applied.
	 * Needed for user interface.
	 */
	private String serviceName;
	/**
	 * The description of the service to show in the user interface, so a user knows what a service is supposed to do.
	 */
	private String serviceDescription;
	private ArrayList<ServiceConfigurationParameter> parameter = new ArrayList<>();
	
	public ServiceConfiguration() {
		// empty constructor needed for JSON serialization 
	}
	
	public ServiceConfiguration(String serviceName) {
		this.serviceName = serviceName;
	}
	
	public String getServiceName() {
		return serviceName;
	}
	
	public void setServiceName(String serviceName) {
		this.serviceName = serviceName;
	}

	public String getServiceDescription() {
		return serviceDescription;
	}

	public void setServiceDescription(String serviceDescription) {
		this.serviceDescription = serviceDescription;
	}

	public void addParameter(ServiceConfigurationParameter param) {
		this.parameter.add(param);
	}
	
	public List<ServiceConfigurationParameter> getParameterList() {
		return this.parameter;
	}
	
	public ServiceConfigurationParameter getParameter(String key) {
		for (ServiceConfigurationParameter param : this.parameter) {
			if (param.getKey().equals(key)) {
				return param;
			}
		}
		return null;
	}
	
}
