/*********************************************************************************
 * Copyright (c) 2020, 2021 Robert Bosch GmbH and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */
package org.eclipse.app4mc.cloud.storage;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Comparator;
import java.util.stream.Stream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class FileSystemStorageService implements StorageService {

	private static final Logger LOG = LoggerFactory.getLogger(FileSystemStorageService.class);
	
	protected static final String TEMP_DIR_PREFIX = "app4mc_mgr_";
	
	protected final String defaultBaseDir = System.getProperty("java.io.tmpdir");
	protected final Path tempLocation = Paths.get(defaultBaseDir);
	
	@Override
	public String store(InputStream input, String originalFilename) {
		try {
			Path path = Files.createTempDirectory(TEMP_DIR_PREFIX);
			
			// extract uuid from pathname
			String uuid = path.toString().substring(path.toString().lastIndexOf('_') + 1);
			
			store(path, input, originalFilename);
			
			return uuid;
		} catch (IOException e) {
			throw new StorageException("Failed to create temporary directory for uploading file " + originalFilename, e);
		}
	}

	@Override
	public void store(String uuid, InputStream input, String originalFilename) {
		Path path = this.tempLocation.resolve(TEMP_DIR_PREFIX + uuid);
		store(path, input, originalFilename);
	}

	private void store(Path path, InputStream input, String originalFilename) {
		try {
			Files.copy(input, path.resolve(originalFilename));
		} catch (IOException e) {
			throw new StorageException("Failed to store file " + originalFilename, e);
		}
	}
	
	@Override
	public Stream<Path> loadAll() {
		try {
			return Files.walk(this.tempLocation, 1)
					.filter(path -> !path.equals(this.tempLocation))
					.filter(path -> path.getFileName().toString().startsWith(TEMP_DIR_PREFIX));
		} catch (IOException e) {
			throw new StorageException("Failed to read stored files", e);
		}

	}

	@Override
	public Path load(String uuid, String... path) {
		Path result = this.tempLocation.resolve(TEMP_DIR_PREFIX + uuid);
		for (String p : path) {
			result = result.resolve(p);
		}
		return result;
	}

	@Override
	public void delete(String uuid) {
		Path path = load(uuid);
		try {
			Files.walk(path)
				.sorted(Comparator.reverseOrder())
				.map(Path::toFile)
				.forEach(File::delete);
		} catch (IOException e) {
			throw new StorageFileNotFoundException("Could not delete: " + path);
		}
	}
	
	@Override
	public void deleteAll() {
		loadAll().forEach(path -> {
			try {
				Files.walk(path)
			      .sorted(Comparator.reverseOrder())
			      .map(Path::toFile)
			      .forEach(File::delete);
			} catch (IOException e) {
				LOG.error("Could not delete: {}", path, e);
			}
		});
	}
	
	public static void zipResult(Path folder) throws IOException {
		Path outputFileName = folder.resolve("result.zip");
	    if (Files.exists(outputFileName)) {
			Files.delete(outputFileName);
	    }
	    Path zipFile = Files.createFile(outputFileName);
		try (ZipOutputStream zipOutputStream = new ZipOutputStream(Files.newOutputStream(zipFile))) {
	        Files.walk(folder)
	        	.filter(path -> !Files.isDirectory(path))
	        	.filter(path -> !path.getFileName().startsWith("."))
	        	.filter(path -> !path.getFileName().endsWith("result.zip"))
	        	.forEach(path -> {
	        		try {
	        			ZipEntry zipEntry = new ZipEntry(folder.relativize(path).toString());
	        			zipOutputStream.putNextEntry(zipEntry);
	        			Files.copy(path, zipOutputStream);
	        			zipOutputStream.closeEntry();
	        		} catch (IOException e) {
	        			LOG.error("Failed to include {} to zip archive", path.getFileName(), e);
	        		}
		       });
		} catch (Exception e){
			LOG.error("Failed to produce result zip archive", e);
		}
	}

}
