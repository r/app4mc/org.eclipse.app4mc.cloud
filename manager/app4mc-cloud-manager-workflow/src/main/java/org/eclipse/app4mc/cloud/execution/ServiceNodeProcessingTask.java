/*********************************************************************************
 * Copyright (c) 2020, 2021 Robert Bosch GmbH and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 *     Dortmund University of Applied Sciences and Arts - Bug 570871
 ********************************************************************************
 */
package org.eclipse.app4mc.cloud.execution;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.ForkJoinTask;
import java.util.concurrent.RecursiveTask;

import org.eclipse.app4mc.cloud.execution.ProcessLog.Action;
import org.eclipse.app4mc.cloud.storage.StorageService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import kong.unirest.HttpResponse;
import kong.unirest.MultipartBody;
import kong.unirest.Unirest;

public class ServiceNodeProcessingTask extends RecursiveTask<Path> {

	private static final long serialVersionUID = 8286285942428911612L;

	private final String uuid;
	private final ServiceNode serviceNode;
	private final Path inputFile;

	private final StorageService storageService;
	private final MessageDispatcher messagingTemplate;
	private WorkflowStatus workflowStatus;

	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	public ServiceNodeProcessingTask(
			ServiceNode n, 
			Path inputFile, 
			StorageService storageService,
			MessageDispatcher messagingTemplate, 
			String uuid, 
			WorkflowStatus ws) {
		
		this.storageService = storageService;
		this.messagingTemplate = messagingTemplate;
		this.workflowStatus = ws;
		this.serviceNode = n;
		this.inputFile = inputFile;
		this.uuid = uuid;
	}

	@Override
	protected Path compute() {
		if (this.serviceNode.isStructuralNode()) {
			logger.info("created structural node thread: {}", this.serviceNode.getId());
			return processStructuralNode(this.serviceNode, this.inputFile);
		} else {
			logger.info("created branch node thread: {}", this.serviceNode.getId());
			Path retval = processNonStructuralNode(this.serviceNode, this.inputFile);
			if (this.workflowStatus.isCancelled()) {
				addMessage(this.workflowStatus, "Workflow cancelled by user");
			}
			return retval;
		}
	}

	private Path processStructuralNode(ServiceNode node, Path inputFile) {
		Path nextInput = inputFile;
		for (ServiceNode childNode : node.getChildren()) {
			nextInput = processNonStructuralNode(childNode, nextInput);
			
			// if an error occurs without throwing an exception, successor tasks do not get executed
			if (childNode.isFailed()) {
				break;
			}
			
			if (this.workflowStatus.isCancelled()) {
				addMessage(this.workflowStatus, "Workflow cancelled by user");
				break;
			}
		}
		return nextInput;
	}

	private Path processNonStructuralNode(ServiceNode node, Path inputFile) {
		
		// first execute the service of the provided node
		Path currOutput = null;
		try {
			currOutput = executeCloudService(node, inputFile);
		} catch (ProcessingFailedException e) {
			addError(this.workflowStatus, e.getMessage(), node);
		}
		
		// check if the service node execution failed
		// if an error occurred without throwing an exception, successor tasks do not get executed
		if (node.isFailed()) {
			return null;
		}
		
		if (node.getChildren().size() > 1) {
			ArrayList<ServiceNodeProcessingTask> childTasks = new ArrayList<>();
			for (ServiceNode childNode : node.getChildren()) {
				childTasks.add(new ServiceNodeProcessingTask(
						childNode, 
						currOutput, 
						storageService,
						messagingTemplate, 
						uuid, 
						workflowStatus));
			}

			logger.info("launching new branches");
			ForkJoinTask.invokeAll(childTasks);
		} else if (node.getChildren().size() == 1) {
			ServiceNode nextNode = node.getChildren().get(0);
			if (nextNode.isStructuralNode()) {
				ServiceNodeProcessingTask nextNodeTask = 
						new ServiceNodeProcessingTask(
								nextNode, 
								currOutput,
								storageService,
								messagingTemplate,
								uuid,
								workflowStatus);
				ForkJoinPool.commonPool().invoke(nextNodeTask);
			} else {
				processNonStructuralNode(nextNode, currOutput);
			}
		}
		
		return currOutput;
	}

	private Path executeCloudService(ServiceNode node, Path inputFile) {
		try {
			CloudServiceDefinition csd = node.getService();
			ServiceConfiguration config = node.getServiceConfiguration(); 
			String serviceName = csd.getName();
			String baseUrl = csd.getBaseUrl();
			
			if (workflowStatus.isCancelled()) {
				return null;
			}
			
			// upload to service
			MultipartBody multipartBody = Unirest.post(baseUrl)
					.field("file", Files.newInputStream(inputFile), inputFile.getFileName().toString());
			
			if (config != null) {
				config.getParameterList().forEach(param -> {
					if ((param.getValue() != null && !"".equals(param.getValue())) && !param.isManagerParameter()) {
						if ("multiple".equals(param.getCardinality())) {
							multipartBody.field(param.getKey(), Arrays.asList(param.getValue().split(",")));
						} else {
							multipartBody.field(param.getKey(), param.getValue());
						}
					}
				});
			}
			
			HttpResponse<?> uploadResponse = multipartBody.asEmpty();
			
			// extract status link from result
			String statusUrl = null;
			if (uploadResponse.getStatus() == 201) {
				statusUrl = uploadResponse.getHeaders().getFirst("Location");
				if (statusUrl == null || "".equals(statusUrl)) {
					// fallback check for Link header if Location header is not set
					statusUrl = HeaderHelper.getUrlFromLink(uploadResponse.getHeaders().get("Link"), "status", baseUrl);
				} else if (!HeaderHelper.isValid(statusUrl, baseUrl)) {
					throw new ProcessingFailedException(
							"The status URL from the Location header does not match with the service base URL '" + baseUrl + "': " + statusUrl);
				}
			} else if (uploadResponse.getStatus() == 200) {
				// fallback if return code is 200, then follow up needs to be placed in Link header
				statusUrl = HeaderHelper.getUrlFromLink(uploadResponse.getHeaders().get("Link"), "status", baseUrl);
			} else {
				// error
				Object body = uploadResponse.getBody();
				if (body != null && !body.toString().isEmpty()) {
					addError(workflowStatus,
							"Upload to " + serviceName + " failed! Error code: " + uploadResponse.getStatus() + " - " + body +" - Workflow stopped!",
							node);
				} else {
					addError(workflowStatus,
							"Upload to " + serviceName + " failed! Error code: " + uploadResponse.getStatus() + " - Workflow stopped!",
							node);
				}
				return null;
			}
			
			addMessage(workflowStatus, "Upload to " + serviceName + " service succeeded");

			Path result = inputFile;
			
			if (workflowStatus.isCancelled()) {
				return null;
			}

			// trigger status URL until process is finished or error occured
			if (statusUrl != null) {
				HttpResponse<?> statusResponse = Unirest.get(statusUrl).asEmpty();
				List<String> linkHeaders = HeaderHelper.getLinkHeaders(statusResponse);
				
				long start = System.currentTimeMillis();
				long end = System.currentTimeMillis();
				
				long timeout = 60_000l;
				if (config != null) {
					ServiceConfigurationParameter timeoutParam = config.getParameter("timeout");
					if (timeoutParam != null) {
						try {
							timeout = Long.valueOf(timeoutParam.getValue());
						} catch (Exception e) {
							addMessage(workflowStatus, serviceName + " timeout value " + timeoutParam.getValue() + " invalid. Using default of 60_000");
							timeout = 60_000l;
						}
					}
				}
				
				while (linkHeaders.size() <= 1) {
					try {
						Thread.sleep(1000);
					} catch (InterruptedException e) {
						// Restore interrupted state...
					    Thread.currentThread().interrupt();
					}

					statusResponse = Unirest.get(statusUrl).asEmpty();
					linkHeaders = HeaderHelper.getLinkHeaders(statusResponse);
					
					end = System.currentTimeMillis();
					
					// don't request for more than configured timeout
					if (timeout > 0 && (end - start) > timeout) {
						addMessage(workflowStatus, serviceName + " status requested for " + (timeout/1000) + " seconds");
						break;
					}
					
					if (workflowStatus.isCancelled()) {
						// TODO trigger cancel at service?
						return null;
					}
					
					// send a processing message every 5 seconds to the client to indicate that the
					// service is still progressing
					if ((((end - start) / 1000) % 5) == 0 && this.messagingTemplate != null) {
						synchronized (this.messagingTemplate) {
								this.messagingTemplate.send(
								"/topic/process-updates/" + this.uuid, 
								new ProcessLog(Action.PROCESSING, serviceName + " is processing ..."));

						}
					}
				}

				// create the service result directory relative to the input file
				Path serviceSubDir = getServiceDir(node);
				Files.createDirectories(serviceSubDir);
				
				String servicePath = storageService.load(workflowStatus.getUuid()).relativize(serviceSubDir).toString();
				// replace backslashes with forward slashes in case the service is executed on Windows
				servicePath = servicePath.replaceAll("\\\\", "/");
				if (!servicePath.isEmpty()) {
					servicePath += "/";
				}
				
				// first check if there is a result link
				List<String> resultUrls = HeaderHelper.getUrlsFromLink(linkHeaders, "result", baseUrl);
				
				String deleteUrl = null;
				if (!resultUrls.isEmpty()) {

					addMessage(workflowStatus, serviceName + " processing finished");
					
					boolean error = false;
					for (String resultUrl : resultUrls) {
						
						// download file
						HttpResponse<File> resultResponse = Unirest.get(resultUrl)
								.asFile(serviceSubDir.resolve("result").toString());
						
						Path migrationResult = resultResponse.getBody().toPath();
						String filename = HeaderHelper.getFilenameFromHeader(resultResponse.getHeaders().getFirst("Content-Disposition"));
						if (filename != null) {
							migrationResult = Files.move(migrationResult, serviceSubDir.resolve(filename));
						}
						
						String resultName = resultResponse.getHeaders().getFirst("x-app4mc-result-name");
						if (resultName == null || "".equals(resultName)) {
							resultName = serviceName;
						}

						addMessage(workflowStatus, resultName + " result downloaded");

						workflowStatus.addResult(
								resultName + " Result",
								servicePath + migrationResult.getFileName().toString());
						
						// extract header information if result should be used in workflow
						String useResult = resultResponse.getHeaders().getFirst("x-app4mc-use-result");
						if (useResult == null || !useResult.toLowerCase().equals("false")) {
							// the result should be used in the workflow
							result = migrationResult;
						}

						if (resultResponse.getStatus() != 200) {
							error = true;
						}
						
						if (deleteUrl == null) {
							// extract delete
							deleteUrl = HeaderHelper.getUrlFromLink(resultResponse.getHeaders().get("Link"), "delete", baseUrl);
						}
					}
					
					if (error) {
						addError(workflowStatus, serviceName + " failed with errors", node);						
					} else {
						addMessage(workflowStatus, serviceName + " successfull");
					}
				} else {
					String errorUrl = HeaderHelper.getUrlFromLink(linkHeaders, "error", baseUrl);
					if (errorUrl != null) {
						addError(workflowStatus, serviceName + " processing finished with error", node);

						// download error file
						HttpResponse<File> errorResponse = Unirest.get(errorUrl)
								.asFile(serviceSubDir.resolve("error").toString());

						Path migrationError = errorResponse.getBody().toPath();
						String filename = HeaderHelper.getFilenameFromHeader(errorResponse.getHeaders().getFirst("Content-Disposition"));
						if (filename != null) {
							migrationError = Files.move(migrationError, serviceSubDir.resolve(filename));
						}

						addMessage(workflowStatus, serviceName + " error result downloaded");
						
						workflowStatus.addResult(
								serviceName + " Error",
								servicePath + migrationError.getFileName().toString());
						
						// extract delete
						deleteUrl = HeaderHelper.getUrlFromLink(errorResponse.getHeaders().get("Link"), "delete", baseUrl);
					} else {
						addError(workflowStatus, serviceName + " has no result and no error", node);
					}
				}

				boolean deleteResult = true;
				if (config != null) {
					ServiceConfigurationParameter deleteParam = config.getParameter("deleteResult");
					if (deleteParam != null) {
						deleteResult = Boolean.valueOf(deleteParam.getValue());
					}
				}

				if (deleteUrl != null && deleteResult) {
					// delete upload again
					Unirest.delete(deleteUrl).asEmpty();
					addMessage(workflowStatus, serviceName + " cleaned up");
				}
				
			} else {
				// no status URL in the upload response, stop further processing
				workflowStatus.addMessage("No status URL found for service " + serviceName);
				return inputFile;
			}
			
			return result;

		} catch (Exception e) {
			throw new ProcessingFailedException("Error in " + node.getService().getName() + " workflow: " + e.getMessage(), e);
		}
	}
	
	private Path getServiceDir(ServiceNode node) {
		Path path = storageService.load(workflowStatus.getUuid());
		ServiceNode sn = node;
		ArrayList<String> segments = new ArrayList<>();
		segments.add(getServiceNodeDirName(sn));
		while (sn.getParentNode() != null && !sn.getParentNode().getId().equals(ServiceNode.ROOT_NODE_ID)) {
			sn = sn.getParentNode();
			segments.add(0, getServiceNodeDirName(sn));
		}
		
		for (String segment : segments) {
			path = path.resolve(segment);
		}
		
		return path;
	}
	
	private String getServiceNodeDirName(ServiceNode node) {
		if (node.getService() != null) {
			return "_" + node.getService().getKey().toLowerCase();
		}
		return "_" + node.getId().toLowerCase();
	}
	
	private void addMessage(WorkflowStatus workflowStatus, String message) {
		if (this.messagingTemplate != null) {
			synchronized(this.messagingTemplate){
				workflowStatus.addMessage(message);
				this.messagingTemplate.send(
						"/topic/process-updates/" + this.uuid, 
						new ProcessLog(Action.UPDATE, message));
			}
		}
	}

	private void addError(WorkflowStatus workflowStatus, String message, ServiceNode node) {
		workflowStatus.addError(message);						
		node.markFailed();
	}
}
