/*********************************************************************************
 * Copyright (c) 2020, 2021 Robert Bosch GmbH and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 *     Dortmund University of Applied Sciences and Arts
 ********************************************************************************
 */
package org.eclipse.app4mc.cloud.execution;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@JsonSerialize(using = WorkflowStatusSerializer.class)
public class WorkflowStatus {

	private static final Logger LOGGER = LoggerFactory.getLogger(WorkflowStatus.class);
	
	private String name;
	private String uuid;
	
	private final ServiceNode rootNode = new ServiceNode(ServiceNode.ROOT_NODE_ID);
	
	private final ArrayList<String> messages = new ArrayList<>();
	private final ArrayList<String> errors = new ArrayList<>();
	private final HashMap<String, String> results = new LinkedHashMap<>();
	
	private boolean cancelled = false;
	private boolean done = false;
	
	private boolean websocketConnected = false;
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	/**
	 * 
	 * @return The unique id of the workflow execution or <code>null</code> if the
	 *         execution was not performed yet.
	 */
	public String getUuid() {
		return uuid;
	}
	
	public void setUuid(String uuid) {
		this.uuid = uuid;
	}
	
	/**
	 * 
	 * @return The root node of the selected services, which is a structural node.
	 */
	public ServiceNode getServiceRootNode() {
		return this.rootNode;
	}
	
	/**
	 * 
	 * @return The services on root level configured for this {@link WorkflowStatus}.
	 */
	public List<ServiceNode> getSelectedServices() {
		return this.rootNode.getChildren();
	}
	
	/**
	 * @param serviceId	The Id of a given service
	 * @return The service in the with given id.
	 */
	public ServiceNode getServiceByQualifiedId(String qualifiedId) {
		String[] keys = qualifiedId.split("\\.");
		ServiceNode parentNode = this.rootNode;
		ServiceNode node = null;
		for (int i = 0; i < keys.length; i++) {
			String key = keys[i];
			node = parentNode.getChildren().stream()
					.filter(child -> child.getId().equals(key))
					.findFirst()
					.orElse(null);
			
			if (node == null) {
				LOGGER.warn("Could not resolve key '{}' in qualifiedId '{}'", key, qualifiedId);
				break;
			}
			
			parentNode = node;
		}
		
		return node;
	}

	/**
	 * Add a {@link CloudServiceDefinition} and its corresponding
	 * {@link ServiceConfiguration} on the root level of the services to execute by
	 * this {@link WorkflowStatus}.
	 * 
	 * @param service The {@link CloudServiceDefinition} to add.
	 * @param config  The {@link ServiceConfiguration} for the given
	 *                {@link CloudServiceDefinition}. Can be <code>null</code>.
	 */
	public void addSelectedService(CloudServiceDefinition service, ServiceConfiguration config) {
		if (this.rootNode.getChildren().stream()
				.noneMatch(child -> child.getId().equals(service.getKey()))) {
			this.rootNode.addChild(new ServiceNode(service, config));
		}
	}
	
	/**
	 * Add a {@link CloudServiceDefinition} and its corresponding
	 * {@link ServiceConfiguration} as a child of an already registered service node
	 * denoted by the given parent key.
	 * 
	 * @param parentId  The qualified Id of the parent - key to identify an existing service node. A service path
	 *                  can be represented as key by concatenating the keys with a
	 *                  dot, e.g. migration.validation if validation is a child of
	 *                  migration. Note: root should not be added as part of the key.
	 * @param service   The {@link CloudServiceDefinition} to add.
	 * @param config    The {@link ServiceConfiguration} for the given
	 *                  {@link CloudServiceDefinition}. Can be <code>null</code>.
	 */
	public void addSelectedService(String parentId, CloudServiceDefinition service, ServiceConfiguration config) {
		String[] keys = parentId.split("\\.");
		ServiceNode parentNode = this.rootNode;
		ServiceNode node = null;
		for (int i = 0; i < keys.length; i++) {
			String key = keys[i];
			node = parentNode.getChildren().stream()
					.filter(child -> child.getId().equals(key))
					.findFirst()
					.orElse(null);
			
			if (i == (keys.length - 1)) {
				if (node != null) {
					// we found a node, so simply add the child
					
					// If the parent service does not already have that service as a child,
					// add the service as a child. This helps us to prevent having 
					// multiple instances of the same service at the same level.
					if (node.getChildren().stream()
							.noneMatch(child -> child.getId().equals(service.getKey()))) {
						node.addChild(new ServiceNode(service, config));
					}
				} else if (node == null) {
					// we did not find the node, so probably a structural node should be added
					node = new ServiceNode(key);
					node.addChild(new ServiceNode(service, config));
					parentNode.addChild(node);
				}
			}
			
			parentNode = node;
		}
	}
	
	/**
	 * Adds all provided {@link ServiceNode}s on root level of this
	 * {@link WorkflowStatus}. Used for copy mechanisms.
	 * 
	 * @param nodes The nodes to add on root level.
	 */
	public void addAllSelectedServices(List<ServiceNode> nodes) {
		nodes.forEach(this.rootNode::addChild);
	}
	
	/**
	 * Removes the given {@link CloudServiceDefinition} from the root level of this
	 * {@link WorkflowStatus}.
	 * 
	 * @param service The {@link CloudServiceDefinition} to remove.
	 */
	public void removeSelectedService(CloudServiceDefinition service) {
		this.rootNode.removeChild(service);
	}
	
	/**
	 * Removes the given {@link ServiceNode} from the root level of this
	 * {@link WorkflowStatus}.
	 * 
	 * @param serviceKey The key of the {@link CloudServiceDefinition} of the {@link ServiceNode} to remove.
	 */
	public void removeSelectedService(String serviceKey) {
		ServiceNode node = this.rootNode.getChildren().stream()
							.filter(child -> child.getId().equals(serviceKey))
							.findFirst()
							.orElse(null);
		if (node != null) {
			this.rootNode.removeChild(node);
		}
	}

	/**
	 * Removes the given {@link CloudServiceDefinition} from the service node
	 * denoted by the given parent key.
	 * 
	 * @param parentKey The key to identify the existing service node from which the
	 *                  given service should be removed. A service path can be
	 *                  represented as key by concatenating the keys with a dot,
	 *                  e.g. migration.validation if validation is a child of
	 *                  migration. Note: root should not be added as part of the
	 *                  key.
	 * @param service   The {@link CloudServiceDefinition} to remove.
	 */
	public void removeSelectedService(String parentKey, CloudServiceDefinition service) {
		ServiceNode node = getServiceNode(parentKey);
		if (node != null) {
			node.removeChild(service);
		}
	}
	
	/**
	 * Removes the given {@link CloudServiceDefinition} from the service node
	 * denoted by the given parent ID.
	 * 
	 * @param parentId  The qualified ID to identify the existing service node from which the
	 *                  given service should be removed. A service path can be
	 *                  represented as key by concatenating the keys with a dot,
	 *                  e.g. migration.validation if validation is a child of
	 *                  migration. Note: root should not be added as part of the
	 *                  key.
	 * @param key   	The key of the {@link ServiceNode} to remove.
	 */
	public void removeSelectedService(String parentId, String serviceKey) {
		String[] keys = parentId.split("\\.");
		ServiceNode parentNode = this.rootNode;
		ServiceNode node = null;
		for (int i = 0; i < keys.length; i++) {
			String key = keys[i];
			node = parentNode.getChildren().stream()
					.filter(child -> child.getId().equals(key))
					.findFirst()
					.orElse(null);
			
			if (i == (keys.length - 1)) {
				if (node != null) {
					node.removeChild(serviceKey);
				}
			}
			
			parentNode = node;
		}
	}
	
	/**
	 * Removes a service node denoted by the given key.
	 * 
	 * @param key The key to identify the service node to remove. A service path can
	 *            be represented as key by concatenating the keys with a dot, e.g.
	 *            migration.validation if validation is a child of migration. Note:
	 *            root should not be added as part of the key.
	 */
	public void removeServiceNode(String key) {
		// find the parent node
		ServiceNode node = key.contains(".") ? getServiceNode(key.substring(0, key.lastIndexOf('.'))) : this.rootNode;
		node.removeChild(key.substring(key.lastIndexOf('.') + 1));
	}
	
	/**
	 * 
	 * @param key The key for which the {@link CloudServiceDefinition} is requested. A
	 *            key is typically the key of a {@link CloudServiceDefinition}. It
	 *            is possible to provide a key path by concatenating keys with a
	 *            dot, e.g. migration.validation in case validation is a child of
	 *            migration.
	 * @return The {@link CloudServiceDefinition} for the specified key or
	 *         <code>null</code> if there is no {@link CloudServiceDefinition} for the
	 *         specified key registered.
	 */
	public CloudServiceDefinition getService(String key) {
		ServiceNode node = getServiceNode(key);
		if (node != null) {
			return node.getService();
		}
		return null;
	}

	/**
	 * 
	 * @param key The key for which the {@link ServiceConfiguration} is requested. A
	 *            key is typically the key of a {@link CloudServiceDefinition}. It
	 *            is possible to provide a key path by concatenating keys with a
	 *            dot, e.g. migration.validation in case validation is a child of
	 *            migration.
	 * @return The {@link ServiceConfiguration} for the specified key or
	 *         <code>null</code> if there is no {@link ServiceConfiguration} for the
	 *         specified key registered.
	 */
	public ServiceConfiguration getConfiguration(String key) {
		ServiceNode node = getServiceNode(key);
		if (node != null) {
			return node.getServiceConfiguration();
		}
		return null;
	}
	
	private ServiceNode getServiceNode(String nodeKey) {
		String[] keys = nodeKey.split("\\.");
		ServiceNode parentNode = this.rootNode;
		ServiceNode node = null;
		for (int i = 0; i < keys.length; i++) {
			String key = keys[i];
			node = parentNode.getChildren().stream()
					.filter(child -> child.getId().equals(key))
					.findFirst()
					.orElse(null);
			
			if (node == null) {
				LOGGER.warn("Could not resolve key '{}' in nodeKey '{}'", key, nodeKey);
				break;
			}
			
			parentNode = node;
		}
		
		return node;		
	}
	
	public List<String> getMessages() {
		return new ArrayList<>(this.messages);
	}
	
	public boolean hasMessages() {
		return !this.messages.isEmpty();
	}
	
	public void addMessage(String message) {
		this.messages.add(message);
	}
	
	public List<String> getErrors() {
		return new ArrayList<>(this.errors);
	}
	
	public boolean hasErrors() {
		return !this.errors.isEmpty();
	}
	
	public void addError(String error) {
		this.errors.add(error);
	}
	
	public HashMap<String, String> getResults() {
		return new LinkedHashMap<>(this.results);
	}
	
	public boolean hasResults() {
		return !this.results.isEmpty();
	}
	
	public void addResult(String key, String resultFile) {
		// TODO maybe implement a smarter mechanism to avoid overriding existing results with a more meaningful name
		String keyToUse = key;
		if (this.results.containsKey(key)) {
			long number = this.results.keySet().stream().filter(k -> k.startsWith(key)).count() + 1;
			keyToUse += " (" + number + ")";
		}
		this.results.put(keyToUse, resultFile);
	}
	
	public boolean isCancelled() {
		return cancelled;
	}

	public void cancel() {
		this.cancelled = true;
	}

	public boolean isDone() {
		return done;
	}

	public void done() {
		this.done = true;
	}

	public void connect() {
		this.websocketConnected = true;
	}
	
	public void disconnect() {
		this.websocketConnected = false;
	}
	
	public boolean isConnected() {
		return this.websocketConnected;
	}
	
	public void clearResults() {
		this.messages.clear();
		this.errors.clear();
		this.results.clear();
	}
	
	public void clear() {
		this.rootNode.clearChildren();
		this.messages.clear();
		this.errors.clear();
		this.results.clear();
	}

}
