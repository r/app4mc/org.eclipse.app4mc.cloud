/*********************************************************************************
 * Copyright (c) 2020, 2021 Robert Bosch GmbH and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 *     Dortmund University of Applied Sciences and Arts
 ********************************************************************************
 */
package org.eclipse.app4mc.cloud.execution;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

public class ServiceNode {
	public static final String ROOT_NODE_ID = "root";

	private final String id;
	private final CloudServiceDefinition service;
	private final ServiceConfiguration config;
	
	private final List<ServiceNode> children = new ArrayList<>();
	
	private ServiceNode parentNode;

	private boolean failed = false;
	/**
	 * Create a structural node like for example the root node.
	 */
	public ServiceNode(String id) {
		if (id == null) {
			throw new IllegalArgumentException("id can not be null!");
		}
		
		this.id = id;
		this.service = null;
		this.config = null;
	}
	
	/**
	 * Create a service node for the given {@link CloudServiceDefinition} and
	 * corresponding {@link ServiceConfiguration}.
	 * 
	 * @param service The {@link CloudServiceDefinition} to store in this node.
	 * @param config  The {@link ServiceConfiguration} for the service.
	 */
	public ServiceNode(CloudServiceDefinition service, ServiceConfiguration config) {
		if (service == null) {
			throw new IllegalArgumentException("service parameter can not be null!");
		}
		this.id = service.getKey();
		this.service = service;
		this.config = config;
	}

	/**
	 * 
	 * @return The id of this node. A custom string for a structural node, the key
	 *         of the {@link CloudServiceDefinition} for a service node.
	 */
	public String getId() {
		return this.id;
	}
	
	/**
	 * 
	 * @return The {@link CloudServiceDefinition} stored in this node.
	 */
	public CloudServiceDefinition getService() {
		return service;
	}

	/**
	 * 
	 * @return The {@link ServiceConfiguration} for the
	 *         {@link CloudServiceDefinition} stored in this node. Can be
	 *         <code>null</code>.
	 */
	public ServiceConfiguration getServiceConfiguration() {
		return config;
	}

	/**
	 * Adds the given {@link ServiceNode} to the local children list.
	 * 
	 * @param node The {@link ServiceNode} to add.
	 */
	public void addChild(ServiceNode node) {
		node.parentNode = this;
		this.children.add(node);
	}

	/**
	 * Adds a new child that references the given {@link CloudServiceDefinition}
	 * with its corresponding {@link ServiceConfiguration}.
	 * 
	 * @param service The {@link CloudServiceDefinition} that should be added as a
	 *                child node. Can not be <code>null</code>.
	 * @param config  The corresponding {@link ServiceConfiguration} that provides
	 *                configurations for the {@link CloudServiceDefinition}. Can be
	 *                <code>null</code>.
	 */
	public void addChild(CloudServiceDefinition service, ServiceConfiguration config) {
		ServiceNode node = new ServiceNode(service, config);
		node.parentNode = this;
		this.children.add(node);
	}
	
	/**
	 * Removes the given {@link ServiceNode} from the local children list.
	 * 
	 * @param node The {@link ServiceNode} to remove.
	 */
	public void removeChild(ServiceNode node) {
		node.parentNode = null;
		this.children.remove(node);
	}
	
	/**
	 * Removes the given {@link ServiceNode} with the given id from the local
	 * children list.
	 * 
	 * @param id The id of the {@link ServiceNode} to remove.
	 */
	public void removeChild(String id) {
		for (Iterator<ServiceNode> it = this.children.iterator(); it.hasNext(); ) {
			ServiceNode child = it.next();
			if (child.getId().equals(id)) {
				child.parentNode = null;
				it.remove();
			}
		}
	}
	
	/**
	 * Removes the {@link ServiceNode} from the local children list that references
	 * the given {@link CloudServiceDefinition}.
	 * 
	 * @param toRemove The {@link CloudServiceDefinition} for which the
	 *                {@link ServiceNode} should be removed.
	 */
	public void removeChild(CloudServiceDefinition toRemove) {
		for (Iterator<ServiceNode> it = this.children.iterator(); it.hasNext(); ) {
			ServiceNode child = it.next();
			if (child.getService().getKey().equals(toRemove.getKey())) {
				child.parentNode = null;
				it.remove();
			}
		}
	}
	
	/**
	 * 
	 * @return An unmodifiable list of the child {@link ServiceNode}s in this node.
	 */
	public List<ServiceNode> getChildren() {
		return Collections.unmodifiableList(this.children);
	}

	/**
	 * Clear the locally mapped services in this node.
	 */
	public void clearChildren() {
		this.children.clear();
	}
	
	/**
	 * 
	 * @return The parent {@link ServiceNode} if this node is a child node.
	 *         <code>null</code> if this node is the root node or this node is
	 *         detached from a tree structure.
	 */
	public ServiceNode getParentNode() {
		return this.parentNode;
	}
	
	/**
	 * 
	 * @return <code>true</code> if this node is only used for structuring (e.g. a
	 *         root node), <code>false</code> if it references a service definition.
	 */
	public boolean isStructuralNode() {
		return this.service == null;
	}
	
	@Override
	public String toString() {
		return this.id;
	}
	
	public void markFailed() {
		this.failed = true;
	}
	
	public boolean isFailed() {
		return this.failed;
	}
	
	public String getQualifiedId() {
		if (this.parentNode == null || this.parentNode.getId().equals(ROOT_NODE_ID)) {
			return this.id;
		}
		return this.parentNode.getQualifiedId() + "." + this.id;
	}
}
