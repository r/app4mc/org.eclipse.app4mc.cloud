/*********************************************************************************
 * Copyright (c) 2020, 2021 Robert Bosch GmbH and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */
package org.eclipse.app4mc.cloud.execution;

import java.io.File;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;

import kong.unirest.HttpResponse;
import kong.unirest.JsonNode;
import kong.unirest.Unirest;

public final class WorkflowStatusHelper {

	private static Logger LOG = LoggerFactory.getLogger(WorkflowStatusHelper.class);
	
	private WorkflowStatusHelper() {
		// private constructor for helper class
	}
	
	/**
	 * Get the {@link ServiceConfiguration} for the given
	 * {@link CloudServiceDefinition}.
	 * 
	 * @param csd The {@link CloudServiceDefinition} for which the
	 *            {@link ServiceConfiguration} is requested.
	 * @return The {@link ServiceConfiguration} for the given
	 *         {@link CloudServiceDefinition}.
	 */
	public static ServiceConfiguration getConfigurationForService(CloudServiceDefinition csd) {
		String selected = csd.getKey();
		ServiceConfiguration config = new ServiceConfiguration(csd.getName());

		if (csd.isConfigurationAvailable()) {
			try {
				String configUrl = csd.getBaseUrl();
				if (!configUrl.endsWith("/")) {
					configUrl += "/";
				}
				configUrl += "config";
				HttpResponse<JsonNode> configResponse = Unirest.get(configUrl).asJson();
				if (configResponse.getStatus() == 200) {
					JsonNode configJson = configResponse.getBody();
					if (configJson != null) {
						ServiceConfigurationDefinition configDefinition = WorkflowStatusHelper.loadServiceConfigurationDefinition(configJson.toString());
						configDefinition.getParameterList().forEach(p -> config.addParameter(p));
						if (configDefinition.getDescription() != null) {
							config.setServiceDescription(configDefinition.getDescription());
						}
					} else {
						LOG.info("Failed to access configuration resource for " + csd.getName() + ": No JSON body in response");
					}
				} else {
					LOG.info("Failed to access configuration resource for " + csd.getName() + ": " + configResponse.getStatusText());
				}
			} catch (Exception e) {
				LOG.info("Failed to access configuration resource for " + csd.getName() + ": " + e.getMessage());
			}
		}
		
		if (csd.getDescription() != null && !"".equals(csd.getDescription())) {
			// service description is provided in the manager administration panel, so we
			// override the value provided by the service itself
			config.setServiceDescription(csd.getDescription());
		}
		
		// specify default configuration parameter for the manager
		ServiceConfigurationParameter timeOut = new ServiceConfigurationParameter();
		timeOut.setName("Timeout (in ms)");
		timeOut.setDescription("The number of milliseconds the workflow should wait for the service to finish.\n-1 means to wait infinitely.");
		timeOut.setKey("timeout");
		timeOut.setValue("60000");
		if ("app4mc_sim".equals(selected)) {
			timeOut.setValue("-1");
		}
		timeOut.setType("Long");
		timeOut.setManagerParameter(true);
		config.addParameter(timeOut);
		
		ServiceConfigurationParameter deleteResult = new ServiceConfigurationParameter();
		deleteResult.setName("Delete result ");
		deleteResult.setDescription("Flag to configure if the result should be deleted in the cloud service.\nDefault is true to keep the infrastructure clean.");
		deleteResult.setKey("deleteResult");
		deleteResult.setValue("true");
		deleteResult.setType("boolean");
		deleteResult.setManagerParameter(true);
		config.addParameter(deleteResult);
		
		return config;
	}
	
	/**
	 * Helper method that adds default configuration parameter to a
	 * {@link WorkflowStatus} in case they are not set.
	 * 
	 * @param workflowStatus The {@link WorkflowStatus} that should be enriched by
	 *                       default configuration parameter.
	 */
	public static void addDefaultConfigurations(WorkflowStatus workflowStatus) {
		for (ServiceNode node : workflowStatus.getSelectedServices()) {
			addDefaultConfigurations(node);
		}
	}

	/**
	 * Helper method that adds default configuration parameter to a
	 * {@link ServiceNode} in case they are not set.
	 * 
	 * @param node The {@link ServiceNode} that should be enriched by
	 *                       default configuration parameter.
	 */
	public static void addDefaultConfigurations(ServiceNode node) {
		// check if a configuration is already available
		ServiceConfiguration configuration = node.getServiceConfiguration();
		
		// configuration can be null for structural nodes
		if (configuration != null) {
			ServiceConfigurationParameter timeOut = configuration.getParameter("timeout");
			if (timeOut == null) {
				timeOut = new ServiceConfigurationParameter();
				timeOut.setName("Timeout (in ms)");
				timeOut.setKey("timeout");
				timeOut.setValue("60000");
				if ("app4mc_sim".equals(node.getService().getKey())) {
					timeOut.setValue("-1");
				}
				timeOut.setType("Long");
				timeOut.setManagerParameter(true);
				configuration.addParameter(timeOut);
			}
			
			ServiceConfigurationParameter deleteResult = configuration.getParameter("deleteResult");
			if (deleteResult == null) {
				deleteResult = new ServiceConfigurationParameter();
				deleteResult.setName("Delete result ");
				deleteResult.setKey("deleteResult");
				deleteResult.setValue("true");
				deleteResult.setType("boolean");
				deleteResult.setManagerParameter(true);
				configuration.addParameter(deleteResult);
			}
		}
		
		for (ServiceNode child : node.getChildren()) {
			addDefaultConfigurations(child);
		}
	}

	/**
	 * Serializes the given {@link WorkflowStatus} to the provided {@link File} as JSON.
	 * 
	 * @param ws   The {@link WorkflowStatus} to serialize.
	 * @param file The {@link File} to write to.
	 */
	public static void saveWorkflowStatus(WorkflowStatus ws, File file) {
		try {
			ObjectMapper mapper = new ObjectMapper();
			mapper.writerWithDefaultPrettyPrinter().writeValue(file, ws);
		} catch (Exception e) {
			LOG.error("Failed to serialize workflow status", e);
		}
		
	}

	/**
	 * Serializes the given {@link WorkflowStatus} as JSON.
	 * 
	 * @param ws The {@link WorkflowStatus} to serialize.
	 * @return The JSON representation of the {@link WorkflowStatus}.
	 * @throws JsonProcessingException 
	 */
	public static String getWorkflowStatusAsJSON(WorkflowStatus ws) throws JsonProcessingException {
		ObjectMapper mapper = new ObjectMapper();
		return mapper.writerWithDefaultPrettyPrinter().writeValueAsString(ws);
	}

	/**
	 * Deserializes the given JSON {@link File}. Will load the service details of
	 * the executed services.
	 * 
	 * @param file                    The {@link File} to deserialize.
	 * @param cloudServiceDefinitions The {@link CloudServiceDefinition}s needed to
	 *                                resolve the used services.
	 * @return The deserialized {@link WorkflowStatus}.
	 */
	public static WorkflowStatus loadWorkflowStatus(File file, List<CloudServiceDefinition> cloudServiceDefinitions) {
		return loadWorkflowStatus(file, cloudServiceDefinitions, true);
	}
	
	/**
	 * Deserializes the given JSON {@link File}.
	 * 
	 * @param file                    The {@link File} to deserialize.
	 * @param cloudServiceDefinitions The {@link CloudServiceDefinition}s needed to
	 *                                resolve the used services.
	 * @param loadServiceDetails      <code>true</code> if the service details
	 *                                should be loaded, <code>false</code> if not.
	 * @return The deserialized {@link WorkflowStatus}.
	 */
	public static WorkflowStatus loadWorkflowStatus(File file, List<CloudServiceDefinition> cloudServiceDefinitions, boolean loadServiceDetails) {
		try {
			if (file.exists()) {
				ObjectMapper mapper = new ObjectMapper();
				
				SimpleModule module = new SimpleModule();
				module.addDeserializer(WorkflowStatus.class, new WorkflowStatusDeserializer(cloudServiceDefinitions, loadServiceDetails));
				mapper.registerModule(module);
				
				return mapper.readValue(file, WorkflowStatus.class);
			}
		} catch (Exception e) {
			LOG.error("Failed to load workflow status", e);
		}
		return null;
	}
	
	/**
	 * Deserializes the given JSON String.
	 * 
	 * @param input The String to deserialize.
	 * @return The deserialized {@link ServiceConfigurationDefinition}.
	 */
	public static ServiceConfigurationDefinition loadServiceConfigurationDefinition(String input) {
		try {
			ObjectMapper mapper = new ObjectMapper();
			
			SimpleModule module = new SimpleModule();
			mapper.registerModule(module);
			
			return mapper.readValue(input, ServiceConfigurationDefinition.class);
		} catch (Exception e) {
			LOG.error("Failed to load service configuration definition", e);
		}
		return null;
	}
}
