/*********************************************************************************
 * Copyright (c) 2020, 2021 Robert Bosch GmbH and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */
package org.eclipse.app4mc.cloud.execution;

public class CloudServiceDefinition {
	
	String key;
	String name;
	String baseUrl;
	String description;
	boolean configurationAvailable = true;
	
	public CloudServiceDefinition() {
		// empty default constructor
	}
	
	public CloudServiceDefinition(String key, String name, String baseUrl, String description) {
		this(key, name, baseUrl, description, true);
	}
	
	public CloudServiceDefinition(String key, String name, String baseUrl, String description, boolean configAvailable) {
		this.key = key;
		this.name = name;
		this.baseUrl = baseUrl;
		this.description = description;
		this.configurationAvailable = configAvailable;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String getBaseUrl() {
		return baseUrl;
	}
	
	public void setBaseUrl(String baseUrl) {
		this.baseUrl = baseUrl;
	}
	
	public String getDescription() {
		return description;
	}
	
	public void setDescription(String description) {
		this.description = description;
	}

	public boolean isConfigurationAvailable() {
		return configurationAvailable;
	}

	public void setConfigurationAvailable(boolean configurationAvailable) {
		this.configurationAvailable = configurationAvailable;
	}
	
}
