/*********************************************************************************
 * Copyright (c) 2020, 2021 Robert Bosch GmbH and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 *     Dortmund University of Applied Sciences and Arts - Bug 570871
 ********************************************************************************
 */
package org.eclipse.app4mc.cloud.execution;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;

public class WorkflowStatusHelperTest {

	private static String JSON_STRING_SIMPLE_CHAIN;
	private static String JSON_STRING_ALL_NESTED;
	private static String JSON_STRING_STRUCTURED_NESTED;
	
	private static CloudServiceDefinition MIGRATION = new CloudServiceDefinition(
			"migration", 
			"Migration", 
			"http://localhost:8080/app4mc/converter/", 
			"Migrates an input model file to model version 0.9.9",
			false);
	private static CloudServiceDefinition VALIDATION = new CloudServiceDefinition(
			"validation", 
			"Validation", 
			"http://localhost:8181/app4mc/validation/", 
			"Validates the input model file",
			false);

	private static CloudServiceDefinition LABEL_CORE = new CloudServiceDefinition(
			"label_core_interpreter", 
			"Label per Core Interpreter", 
			"http://localhost:8084/app4mc/interpreter/label-per-core/", 
			"Parses an Amalthea model to inspect label access operations per core",
			false);
	private static CloudServiceDefinition LABEL_MEMORY = new CloudServiceDefinition(
			"label_memory_interpreter", 
			"Label per Memory Interpreter", 
			"http://localhost:8084/app4mc/interpreter/label-per-memory/", 
			"Parses an Amalthea model to inspect label access operations per memory",
			false);
	private static CloudServiceDefinition LABEL_TASK = new CloudServiceDefinition(
			"label_task_interpreter", 
			"Label per Task Interpreter", 
			"http://localhost:8084/app4mc/interpreter/label-per-task/", 
			"Parses an Amalthea model to inspect label access operations per task",
			false);
	private static CloudServiceDefinition LABEL_SIZE = new CloudServiceDefinition(
			"label_size_interpreter", 
			"Label Size Interpreter", 
			"http://localhost:8084/app4mc/interpreter/label-size/", 
			"Parses an Amalthea model to inspect the number of labels grouped by size",
			false);
	
	private static CloudServiceDefinition CHART_VISUALIZER = new CloudServiceDefinition(
			"chart_visualizer", 
			"Chart Visualizer", 
			"http://localhost:8083/app4mc/visualization/barchart/", 
			"Visualization of interpreter results",
			false);

	@BeforeAll
	public static void beforeAll() {
		StringBuilder simpleChainBuilder = new StringBuilder();
		simpleChainBuilder.append("{").append(System.lineSeparator());
		simpleChainBuilder.append("  \"name\" : \"Test Name\",").append(System.lineSeparator());
		simpleChainBuilder.append("  \"uuid\" : \"1234\",").append(System.lineSeparator());
		simpleChainBuilder.append("  \"services\" : {").append(System.lineSeparator());
		simpleChainBuilder.append("    \"label_core_interpreter\" : {").append(System.lineSeparator());
		simpleChainBuilder.append("      \"deleteResult\" : \"false\",").append(System.lineSeparator());
		simpleChainBuilder.append("      \"timeout\" : \"360000\"").append(System.lineSeparator());
		simpleChainBuilder.append("    },").append(System.lineSeparator());
		simpleChainBuilder.append("    \"chart_visualizer\" : { }").append(System.lineSeparator());
		simpleChainBuilder.append("  },").append(System.lineSeparator());
		simpleChainBuilder.append("  \"cancelled\" : false,").append(System.lineSeparator());
		simpleChainBuilder.append("  \"done\" : true,").append(System.lineSeparator());
		simpleChainBuilder.append("  \"messages\" : [ \"Message One\", \"Message Two\", \"Message Three\" ],").append(System.lineSeparator());
		simpleChainBuilder.append("  \"results\" : {").append(System.lineSeparator());
		simpleChainBuilder.append("    \"Label per Core Interpreter Result\" : \"_label per core interpreter/nodesdata.json\",").append(System.lineSeparator());
		simpleChainBuilder.append("    \"Chart Visualizer Result\" : \"_chart visualizer/barchart.html\"").append(System.lineSeparator());
		simpleChainBuilder.append("  }").append(System.lineSeparator());
		simpleChainBuilder.append("}");
		
		JSON_STRING_SIMPLE_CHAIN = simpleChainBuilder.toString();

		StringBuilder allNestedBuilder = new StringBuilder();
		allNestedBuilder.append("{").append(System.lineSeparator());
		allNestedBuilder.append("  \"services\" : {").append(System.lineSeparator());
		allNestedBuilder.append("    \"migration\" : {").append(System.lineSeparator());
		allNestedBuilder.append("      \"deleteResult\" : \"true\",").append(System.lineSeparator());
		allNestedBuilder.append("      \"timeout\" : \"60000\",").append(System.lineSeparator());
		allNestedBuilder.append("      \"validation\" : {").append(System.lineSeparator());
		allNestedBuilder.append("        \"deleteResult\" : \"true\",").append(System.lineSeparator());
		allNestedBuilder.append("        \"timeout\" : \"60000\",").append(System.lineSeparator());
		allNestedBuilder.append("        \"label_core_interpreter\" : { }").append(System.lineSeparator());
		allNestedBuilder.append("      }").append(System.lineSeparator());
		allNestedBuilder.append("    }").append(System.lineSeparator());
		allNestedBuilder.append("  }").append(System.lineSeparator());
		allNestedBuilder.append("}");
		
		JSON_STRING_ALL_NESTED = allNestedBuilder.toString();
		
		StringBuilder structuredNestedBuilder = new StringBuilder();
		structuredNestedBuilder.append("{").append(System.lineSeparator());
		structuredNestedBuilder.append("  \"name\" : \"Test Name\",").append(System.lineSeparator());
		structuredNestedBuilder.append("  \"uuid\" : \"1234\",").append(System.lineSeparator());
		structuredNestedBuilder.append("  \"services\" : {").append(System.lineSeparator());
		structuredNestedBuilder.append("    \"migration\" : {").append(System.lineSeparator());
		structuredNestedBuilder.append("      \"deleteResult\" : \"true\",").append(System.lineSeparator());
		structuredNestedBuilder.append("      \"timeout\" : \"60000\"").append(System.lineSeparator());
		structuredNestedBuilder.append("    },").append(System.lineSeparator());
		structuredNestedBuilder.append("    \"validation\" : {").append(System.lineSeparator());
		structuredNestedBuilder.append("      \"deleteResult\" : \"true\",").append(System.lineSeparator());
		structuredNestedBuilder.append("      \"profiles\" : \"Amalthea Standard Validations\",").append(System.lineSeparator());
		structuredNestedBuilder.append("      \"timeout\" : \"60000\",").append(System.lineSeparator());
		structuredNestedBuilder.append("      \"label_core_chart\" : {").append(System.lineSeparator());
		structuredNestedBuilder.append("        \"label_core_interpreter\" : {").append(System.lineSeparator());
		structuredNestedBuilder.append("          \"deleteResult\" : \"false\",").append(System.lineSeparator());
		structuredNestedBuilder.append("          \"timeout\" : \"360000\"").append(System.lineSeparator());
		structuredNestedBuilder.append("        },").append(System.lineSeparator());
		structuredNestedBuilder.append("        \"chart_visualizer\" : { }").append(System.lineSeparator());
		structuredNestedBuilder.append("      },").append(System.lineSeparator());
		structuredNestedBuilder.append("      \"label_memory_chart\" : {").append(System.lineSeparator());
		structuredNestedBuilder.append("        \"label_memory_interpreter\" : {").append(System.lineSeparator());
		structuredNestedBuilder.append("          \"deleteResult\" : \"true\",").append(System.lineSeparator());
		structuredNestedBuilder.append("          \"timeout\" : \"60000\"").append(System.lineSeparator());
		structuredNestedBuilder.append("        },").append(System.lineSeparator());
		structuredNestedBuilder.append("        \"chart_visualizer\" : { }").append(System.lineSeparator());
		structuredNestedBuilder.append("      },").append(System.lineSeparator());
		structuredNestedBuilder.append("      \"label_task_chart\" : {").append(System.lineSeparator());
		structuredNestedBuilder.append("        \"label_task_interpreter\" : {").append(System.lineSeparator());
		structuredNestedBuilder.append("          \"deleteResult\" : \"true\",").append(System.lineSeparator());
		structuredNestedBuilder.append("          \"timeout\" : \"60000\"").append(System.lineSeparator());
		structuredNestedBuilder.append("        },").append(System.lineSeparator());
		structuredNestedBuilder.append("        \"chart_visualizer\" : { }").append(System.lineSeparator());
		structuredNestedBuilder.append("      },").append(System.lineSeparator());
		structuredNestedBuilder.append("      \"label_size_chart\" : {").append(System.lineSeparator());
		structuredNestedBuilder.append("        \"label_size_interpreter\" : {").append(System.lineSeparator());
		structuredNestedBuilder.append("          \"deleteResult\" : \"true\",").append(System.lineSeparator());
		structuredNestedBuilder.append("          \"timeout\" : \"60000\"").append(System.lineSeparator());
		structuredNestedBuilder.append("        },").append(System.lineSeparator());
		structuredNestedBuilder.append("        \"chart_visualizer\" : { }").append(System.lineSeparator());
		structuredNestedBuilder.append("      }").append(System.lineSeparator());
		structuredNestedBuilder.append("    }").append(System.lineSeparator());
		structuredNestedBuilder.append("  },").append(System.lineSeparator());
		structuredNestedBuilder.append("  \"cancelled\" : false,").append(System.lineSeparator());
		structuredNestedBuilder.append("  \"done\" : true,").append(System.lineSeparator());
		structuredNestedBuilder.append("  \"messages\" : [ \"Message One\", \"Message Two\", \"Message Three\" ],").append(System.lineSeparator());
		structuredNestedBuilder.append("  \"results\" : {").append(System.lineSeparator());
		structuredNestedBuilder.append("    \"Label per Core Interpreter Result\" : \"_label per core interpreter/nodesdata.json\",").append(System.lineSeparator());
		structuredNestedBuilder.append("    \"Chart Visualizer Result\" : \"_chart visualizer/barchart.html\"").append(System.lineSeparator());
		structuredNestedBuilder.append("  }").append(System.lineSeparator());
		structuredNestedBuilder.append("}");
		
		JSON_STRING_STRUCTURED_NESTED = structuredNestedBuilder.toString();
	}
	
	@Test
	public void shouldSerializeWorkflowStatus() throws JsonProcessingException {
		ServiceConfiguration config1 = WorkflowStatusHelper.getConfigurationForService(LABEL_CORE);
		config1.getParameter("timeout").setValue("360000");
		config1.getParameter("deleteResult").setValue("false");

		ObjectMapper mapper = new ObjectMapper();
		
		WorkflowStatus ws = new WorkflowStatus();
		ws.setName("Test Name");
		ws.setUuid("1234");
		ws.done();
		
		ws.addSelectedService(LABEL_CORE, config1);
		ws.addSelectedService(CHART_VISUALIZER, null);
		
		ws.addMessage("Message One");
		ws.addMessage("Message Two");
		ws.addMessage("Message Three");
		
		ws.addResult("Label per Core Interpreter Result", "_label per core interpreter/nodesdata.json");
		ws.addResult("Chart Visualizer Result", "_chart visualizer/barchart.html");
		
		String json = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(ws);
		
		assertEquals(JSON_STRING_SIMPLE_CHAIN, json);
	}
	
	@Test
	public void shouldDeserializeWorkflowStatus() throws JsonMappingException, JsonProcessingException {
		ObjectMapper mapper = new ObjectMapper();
		SimpleModule module = new SimpleModule();
		module.addDeserializer(WorkflowStatus.class, new WorkflowStatusDeserializer(Arrays.asList(LABEL_CORE, CHART_VISUALIZER)));
		mapper.registerModule(module);
		
		WorkflowStatus ws = mapper.readValue(JSON_STRING_SIMPLE_CHAIN, WorkflowStatus.class);
		
		assertEquals("Test Name", ws.getName());
		assertEquals("1234", ws.getUuid());
		assertFalse(ws.isCancelled());
		assertTrue(ws.isDone());
		
		List<String> messages = ws.getMessages();
		assertNotNull(messages);
		assertEquals(3, messages.size());
		assertEquals("Message One", messages.get(0));
		assertEquals("Message Two", messages.get(1));
		assertEquals("Message Three", messages.get(2));
		
		List<String> errors = ws.getErrors();
		assertNotNull(errors);
		assertEquals(0, errors.size());
		
		HashMap<String,String> results = ws.getResults();
		assertNotNull(results);
		assertEquals(2, results.size());
		assertEquals("_label per core interpreter/nodesdata.json", results.get("Label per Core Interpreter Result"));
		assertEquals("_chart visualizer/barchart.html", results.get("Chart Visualizer Result"));
		
		List<ServiceNode> services = ws.getSelectedServices();
		assertNotNull(services);
		assertEquals(2, services.size());
		assertEquals("label_core_interpreter", services.get(0).getService().getKey());
		assertEquals("Label per Core Interpreter", services.get(0).getService().getName());
		assertEquals("http://localhost:8084/app4mc/interpreter/label-per-core/", services.get(0).getService().getBaseUrl());
		assertEquals("Parses an Amalthea model to inspect label access operations per core", services.get(0).getService().getDescription());
		assertEquals(0, services.get(0).getChildren().size());
		assertEquals("chart_visualizer", services.get(1).getService().getKey());
		assertEquals("Chart Visualizer", services.get(1).getService().getName());
		assertEquals("http://localhost:8083/app4mc/visualization/barchart/", services.get(1).getService().getBaseUrl());
		assertEquals("Visualization of interpreter results", services.get(1).getService().getDescription());
		assertEquals(0, services.get(1).getChildren().size());
		
		assertEquals("360000", ws.getConfiguration("label_core_interpreter").getParameter("timeout").getValue());
		assertEquals("false", ws.getConfiguration("label_core_interpreter").getParameter("deleteResult").getValue());
		// check that the default configuration set as no configuration is in the JSON
		assertEquals("60000", ws.getConfiguration("chart_visualizer").getParameter("timeout").getValue());
		assertEquals("true", ws.getConfiguration("chart_visualizer").getParameter("deleteResult").getValue());
	}
	
	
	@Test
	public void shouldSerializeAllNestedServices() throws JsonProcessingException {
		WorkflowStatus ws = new WorkflowStatus();
		ws.addSelectedService(MIGRATION, WorkflowStatusHelper.getConfigurationForService(MIGRATION));
		ws.addSelectedService("migration", VALIDATION, WorkflowStatusHelper.getConfigurationForService(VALIDATION));
		ws.addSelectedService("migration.validation", LABEL_CORE, null);
		
		ObjectMapper mapper = new ObjectMapper();
		String json = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(ws);
		
		assertEquals(JSON_STRING_ALL_NESTED, json);
	}

	@Test
	public void shouldDeserializeAllNestedServices() throws JsonProcessingException {
		ObjectMapper mapper = new ObjectMapper();
		SimpleModule module = new SimpleModule();
		module.addDeserializer(WorkflowStatus.class, new WorkflowStatusDeserializer(Arrays.asList(MIGRATION, VALIDATION, LABEL_CORE)));
		mapper.registerModule(module);
		
		WorkflowStatus ws = mapper.readValue(JSON_STRING_ALL_NESTED, WorkflowStatus.class);

		List<ServiceNode> services = ws.getSelectedServices();
		assertNotNull(services);
		assertEquals(1, services.size());
		assertEquals("migration", services.get(0).getService().getKey());
		assertEquals(1, services.get(0).getChildren().size());

		ServiceNode child = services.get(0).getChildren().get(0);
		assertEquals("validation", child.getService().getKey());
		assertEquals(1, child.getChildren().size());
		
		child = child.getChildren().get(0);
		assertEquals("label_core_interpreter", child.getService().getKey());
		assertEquals(0, child.getChildren().size());
	}
	
	@Test
	public void shouldSerializeWorkflowStatusStructuredNested() throws JsonProcessingException {
		ServiceConfiguration validationConfig = WorkflowStatusHelper.getConfigurationForService(VALIDATION);
		ServiceConfigurationParameter validationProfiles = new ServiceConfigurationParameter();
		validationProfiles.setName("Validation profiles");
		validationProfiles.setKey("profiles");
		validationProfiles.setType("String");
		validationProfiles.setCardinality("multiple");
		validationProfiles.setPossibleValues(Arrays.asList("Amalthea Standard Validations", "INCHRON Validations"));
		validationConfig.addParameter(validationProfiles);

		validationConfig.getParameter("profiles").setValue("Amalthea Standard Validations");

		ServiceConfiguration labelCoreConfig = WorkflowStatusHelper.getConfigurationForService(LABEL_CORE);
		labelCoreConfig.getParameter("timeout").setValue("360000");
		labelCoreConfig.getParameter("deleteResult").setValue("false");

		ObjectMapper mapper = new ObjectMapper();
		
		WorkflowStatus ws = new WorkflowStatus();
		ws.setName("Test Name");
		ws.setUuid("1234");
		ws.done();
		
		ws.addSelectedService(MIGRATION, WorkflowStatusHelper.getConfigurationForService(MIGRATION));
		ws.addSelectedService(VALIDATION, validationConfig);
		
		// create structural nodes for sub chains
		ws.addSelectedService("validation.label_core_chart", LABEL_CORE, labelCoreConfig);
		ws.addSelectedService("validation.label_core_chart", CHART_VISUALIZER, null);

		ws.addSelectedService("validation.label_memory_chart", LABEL_MEMORY, WorkflowStatusHelper.getConfigurationForService(LABEL_MEMORY));
		ws.addSelectedService("validation.label_memory_chart", CHART_VISUALIZER, null);

		ws.addSelectedService("validation.label_task_chart", LABEL_TASK, WorkflowStatusHelper.getConfigurationForService(LABEL_TASK));
		ws.addSelectedService("validation.label_task_chart", CHART_VISUALIZER, null);
		
		ws.addSelectedService("validation.label_size_chart", LABEL_SIZE, WorkflowStatusHelper.getConfigurationForService(LABEL_SIZE));
		ws.addSelectedService("validation.label_size_chart", CHART_VISUALIZER, null);
		
		
		ws.addMessage("Message One");
		ws.addMessage("Message Two");
		ws.addMessage("Message Three");
		
		ws.addResult("Label per Core Interpreter Result", "_label per core interpreter/nodesdata.json");
		ws.addResult("Chart Visualizer Result", "_chart visualizer/barchart.html");
		
		String json = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(ws);
		
		assertEquals(JSON_STRING_STRUCTURED_NESTED, json);
	}

	@Test
	public void shouldDeserializeWorkflowStatusStructuredNested() throws JsonProcessingException {
		ObjectMapper mapper = new ObjectMapper();
		SimpleModule module = new SimpleModule();
		module.addDeserializer(
				WorkflowStatus.class, 
				new WorkflowStatusDeserializer(Arrays.asList(MIGRATION, VALIDATION, LABEL_CORE, LABEL_MEMORY, LABEL_TASK, LABEL_SIZE, CHART_VISUALIZER)));
		mapper.registerModule(module);
		
		WorkflowStatus ws = mapper.readValue(JSON_STRING_STRUCTURED_NESTED, WorkflowStatus.class);

		List<ServiceNode> services = ws.getSelectedServices();
		assertNotNull(services);
		assertEquals(2, services.size());
		assertEquals("migration", services.get(0).getService().getKey());
		assertEquals(0, services.get(0).getChildren().size());

		assertEquals("validation", services.get(1).getService().getKey());
		// validation has 4 structure nodes for 4 different sub process chains
		assertEquals(4, services.get(1).getChildren().size());

		ServiceNode child = services.get(1).getChildren().get(0);
		assertTrue(child.isStructuralNode(), "child is not a structural node");
		assertEquals("label_core_chart", child.getId());
		assertEquals(2, child.getChildren().size());
		
		ServiceNode subChild = child.getChildren().get(0);
		assertFalse(subChild.isStructuralNode(), "child is a structural node");
		assertNotNull(subChild.getService());
		assertEquals("label_core_interpreter", subChild.getId());
		assertEquals("label_core_interpreter", subChild.getService().getKey());
		assertEquals("360000", subChild.getServiceConfiguration().getParameter("timeout").getValue());
		assertEquals("false", subChild.getServiceConfiguration().getParameter("deleteResult").getValue());

		subChild = child.getChildren().get(1);
		assertFalse(subChild.isStructuralNode(), "child is a structural node");
		assertNotNull(subChild.getService());
		assertEquals("chart_visualizer", subChild.getId());
		assertEquals("chart_visualizer", subChild.getService().getKey());
		assertEquals("60000", subChild.getServiceConfiguration().getParameter("timeout").getValue());
		assertEquals("true", subChild.getServiceConfiguration().getParameter("deleteResult").getValue());
		
		child = services.get(1).getChildren().get(1);
		assertTrue(child.isStructuralNode(), "child is not a structural node");
		assertEquals("label_memory_chart", child.getId());
		assertEquals(2, child.getChildren().size());
		
		subChild = child.getChildren().get(0);
		assertFalse(subChild.isStructuralNode(), "child is a structural node");
		assertNotNull(subChild.getService());
		assertEquals("label_memory_interpreter", subChild.getId());
		assertEquals("label_memory_interpreter", subChild.getService().getKey());
		assertEquals("60000", subChild.getServiceConfiguration().getParameter("timeout").getValue());
		assertEquals("true", subChild.getServiceConfiguration().getParameter("deleteResult").getValue());
		
		subChild = child.getChildren().get(1);
		assertFalse(subChild.isStructuralNode(), "child is a structural node");
		assertNotNull(subChild.getService());
		assertEquals("chart_visualizer", subChild.getId());
		assertEquals("chart_visualizer", subChild.getService().getKey());
		assertEquals("60000", subChild.getServiceConfiguration().getParameter("timeout").getValue());
		assertEquals("true", subChild.getServiceConfiguration().getParameter("deleteResult").getValue());
		
		child = services.get(1).getChildren().get(2);
		assertTrue(child.isStructuralNode(), "child is not a structural node");
		assertEquals("label_task_chart", child.getId());
		assertEquals(2, child.getChildren().size());
		
		subChild = child.getChildren().get(0);
		assertFalse(subChild.isStructuralNode(), "child is a structural node");
		assertNotNull(subChild.getService());
		assertEquals("label_task_interpreter", subChild.getId());
		assertEquals("label_task_interpreter", subChild.getService().getKey());
		assertEquals("60000", subChild.getServiceConfiguration().getParameter("timeout").getValue());
		assertEquals("true", subChild.getServiceConfiguration().getParameter("deleteResult").getValue());
		
		subChild = child.getChildren().get(1);
		assertFalse(subChild.isStructuralNode(), "child is a structural node");
		assertNotNull(subChild.getService());
		assertEquals("chart_visualizer", subChild.getId());
		assertEquals("chart_visualizer", subChild.getService().getKey());
		assertEquals("60000", subChild.getServiceConfiguration().getParameter("timeout").getValue());
		assertEquals("true", subChild.getServiceConfiguration().getParameter("deleteResult").getValue());
		
		child = services.get(1).getChildren().get(3);
		assertTrue(child.isStructuralNode(), "child is not a structural node");
		assertEquals("label_size_chart", child.getId());
		assertEquals(2, child.getChildren().size());
		
		subChild = child.getChildren().get(0);
		assertFalse(subChild.isStructuralNode(), "child is a structural node");
		assertNotNull(subChild.getService());
		assertEquals("label_size_interpreter", subChild.getId());
		assertEquals("label_size_interpreter", subChild.getService().getKey());
		assertEquals("60000", subChild.getServiceConfiguration().getParameter("timeout").getValue());
		assertEquals("true", subChild.getServiceConfiguration().getParameter("deleteResult").getValue());
		
		subChild = child.getChildren().get(1);
		assertFalse(subChild.isStructuralNode(), "child is a structural node");
		assertNotNull(subChild.getService());
		assertEquals("chart_visualizer", subChild.getId());
		assertEquals("chart_visualizer", subChild.getService().getKey());
		assertEquals("60000", subChild.getServiceConfiguration().getParameter("timeout").getValue());
		assertEquals("true", subChild.getServiceConfiguration().getParameter("deleteResult").getValue());
	}

	@Test
	public void shouldOperateOnWorkflowStatusStructuredNested() throws JsonProcessingException {
		ObjectMapper mapper = new ObjectMapper();
		SimpleModule module = new SimpleModule();
		module.addDeserializer(
				WorkflowStatus.class, 
				new WorkflowStatusDeserializer(Arrays.asList(MIGRATION, VALIDATION, LABEL_CORE, LABEL_MEMORY, LABEL_TASK, LABEL_SIZE, CHART_VISUALIZER)));
		mapper.registerModule(module);
		
		WorkflowStatus ws = mapper.readValue(JSON_STRING_STRUCTURED_NESTED, WorkflowStatus.class);
		
		// test direct access on services via workflow status using concatenated keys
		CloudServiceDefinition service = ws.getService("validation.label_core_chart.chart_visualizer");
		assertEquals(CHART_VISUALIZER, service);
		service = ws.getService("validation.label_memory_chart.chart_visualizer");
		assertEquals(CHART_VISUALIZER, service);
		service = ws.getService("validation.label_task_chart.chart_visualizer");
		assertEquals(CHART_VISUALIZER, service);
		service = ws.getService("validation.label_size_chart.chart_visualizer");
		assertEquals(CHART_VISUALIZER, service);
		
		assertNull(ws.getService("some.stupid.key"));
		
		ServiceConfiguration config = ws.getConfiguration("validation.label_core_chart.label_core_interpreter");
		assertEquals("360000", config.getParameter("timeout").getValue());
		assertEquals("false", config.getParameter("deleteResult").getValue());
		
		assertNull(ws.getConfiguration("some.stupid.key"));
		
		List<ServiceNode> services = ws.getSelectedServices();
		assertNotNull(services);
		assertEquals(2, services.size());
		assertEquals("migration", services.get(0).getService().getKey());

		// remove migration on root level
		ws.removeSelectedService(MIGRATION);
		
		services = ws.getSelectedServices();
		assertNotNull(services);
		assertEquals(1, services.size());
		assertEquals("validation", services.get(0).getService().getKey());
		assertEquals(4, services.get(0).getChildren().size());
		
		// remove structural node label_task_chart
		ws.removeServiceNode("validation.label_task_chart");
		
		assertEquals(3, services.get(0).getChildren().size());
		
		ServiceNode serviceNode = services.get(0).getChildren().get(2);
		assertTrue(serviceNode.isStructuralNode());
		assertEquals("label_size_chart", serviceNode.getId());
		assertEquals(2, serviceNode.getChildren().size());

		// remove chart from label_size_chart
		ws.removeSelectedService("validation.label_size_chart", CHART_VISUALIZER);
//		ws.removeServiceNode("validation.label_size_chart.chart_visualizer");

		assertEquals(1, serviceNode.getChildren().size());
	}
	
	@Test
	public void shouldDeserializeMigrationServiceDefinition() throws IOException {
		Path resourceDirectory = Paths.get("src", "test", "resources", "conf_def_migration.json");
		StringBuilder builder = new StringBuilder();
	    try (BufferedReader br = new BufferedReader(new InputStreamReader(Files.newInputStream(resourceDirectory)))) {
	        String line;
	        while ((line = br.readLine()) != null) {
	            builder.append(line).append("\n");
	        }
	    }
	    
	    ServiceConfigurationDefinition definition = 
	    		WorkflowStatusHelper.loadServiceConfigurationDefinition(builder.toString());
	    
	    assertEquals("Migrates an input model file to model version 1.0.0", definition.getDescription());
	    
	    assertEquals("amxmi", definition.getInputType());
	    assertNull(definition.getInputVersion());
	    assertTrue(definition.isInputArchiveSupported());

	    assertEquals("amxmi", definition.getOutputType());
	    assertEquals("1.0.0", definition.getOutputVersion());
	    assertTrue(definition.isOutputArchiveSupported());
	    
	    assertEquals(1, definition.getParameterList().size());
	    
	    ServiceConfigurationParameter versionParam = definition.getParameter("version");
	    assertNotNull(versionParam);
	    assertEquals("version", versionParam.getKey());
	    assertEquals("Output Model Version", versionParam.getName());
	    assertEquals("The model version to which the input should be migrated to", versionParam.getDescription());
	    assertEquals("1.0.0", versionParam.getValue());
	    assertEquals("String", versionParam.getType());
	    assertEquals("single", versionParam.getCardinality());
	    assertFalse(versionParam.isMandatory());
	    assertFalse(versionParam.isManagerParameter());
	    
	    List<String> possibleValues = versionParam.getPossibleValues();
	    assertNotNull(possibleValues);
	    assertEquals(4, possibleValues.size());
	    assertTrue(possibleValues.contains("1.0.0"));
	    assertTrue(possibleValues.contains("0.9.9"));
	    assertTrue(possibleValues.contains("0.9.8"));
	    assertTrue(possibleValues.contains("0.9.7"));
	}
	
	@Test
	public void shouldDeserializeValidationServiceDefinition() throws IOException {
		Path resourceDirectory = Paths.get("src", "test", "resources", "conf_def_validation.json");
		StringBuilder builder = new StringBuilder();
		try (BufferedReader br = new BufferedReader(new InputStreamReader(Files.newInputStream(resourceDirectory)))) {
			String line;
			while ((line = br.readLine()) != null) {
				builder.append(line).append("\n");
			}
		}
		
		ServiceConfigurationDefinition definition = 
				WorkflowStatusHelper.loadServiceConfigurationDefinition(builder.toString());
		
		assertEquals("amxmi", definition.getInputType());
		assertEquals("1.0.0", definition.getInputVersion());
		assertTrue(definition.isInputArchiveSupported());
		
		assertNull(definition.getOutputType());
		assertNull(definition.getOutputVersion());
		assertFalse(definition.isOutputArchiveSupported());
		
		assertEquals(1, definition.getParameterList().size());
		
		ServiceConfigurationParameter profilesParam = definition.getParameter("profiles");
		assertNotNull(profilesParam);
		assertEquals("profiles", profilesParam.getKey());
		assertEquals("Validation profiles", profilesParam.getName());
		assertNull(profilesParam.getValue());
		assertEquals("String", profilesParam.getType());
		assertEquals("multiple", profilesParam.getCardinality());
		assertFalse(profilesParam.isMandatory());
		assertFalse(profilesParam.isManagerParameter());
		
		List<String> possibleValues = profilesParam.getPossibleValues();
		assertNotNull(possibleValues);
		assertEquals(3, possibleValues.size());
		assertTrue(possibleValues.contains("Amalthea"));
		assertTrue(possibleValues.contains("INCHRON"));
		assertTrue(possibleValues.contains("Timing Architects"));
	}
	
	@Test
	public void shouldDeserializeRtcServiceDefinition() throws IOException {
		Path resourceDirectory = Paths.get("src", "test", "resources", "conf_def_rtc.json");
		StringBuilder builder = new StringBuilder();
		try (BufferedReader br = new BufferedReader(new InputStreamReader(Files.newInputStream(resourceDirectory)))) {
			String line;
			while ((line = br.readLine()) != null) {
				builder.append(line).append("\n");
			}
		}
		
		ServiceConfigurationDefinition definition = 
				WorkflowStatusHelper.loadServiceConfigurationDefinition(builder.toString());
		
		assertEquals("amxmi", definition.getInputType());
		assertEquals("1.0.0", definition.getInputVersion());
		assertFalse(definition.isInputArchiveSupported());
		
		assertEquals("json", definition.getOutputType());
		assertNull(definition.getOutputVersion());
		assertFalse(definition.isOutputArchiveSupported());
		
		assertEquals(4, definition.getParameterList().size());
		
		ServiceConfigurationParameter param = definition.getParameter("analysis-ascending-priorities");
		assertNotNull(param);
		assertEquals("analysis-ascending-priorities", param.getKey());
		assertEquals("Ascending priorities", param.getName());
		assertNull(param.getValue());
		assertEquals("boolean", param.getType());
		assertEquals("single", param.getCardinality());
		assertFalse(param.isMandatory());
		assertFalse(param.isManagerParameter());
		
		param = definition.getParameter("analysis-enable-flows");
		assertNotNull(param);
		assertEquals("analysis-enable-flows", param.getKey());
		assertEquals("Include flow analysis", param.getName());
		assertNull(param.getValue());
		assertEquals("boolean", param.getType());
		assertEquals("single", param.getCardinality());
		assertFalse(param.isMandatory());
		assertFalse(param.isManagerParameter());
		
		param = definition.getParameter("analysis-ignore-overutilization");
		assertNotNull(param);
		assertEquals("analysis-ignore-overutilization", param.getKey());
		assertEquals("Ignore over utilization", param.getName());
		assertNull(param.getValue());
		assertEquals("boolean", param.getType());
		assertEquals("single", param.getCardinality());
		assertFalse(param.isMandatory());
		assertFalse(param.isManagerParameter());
		
		param = definition.getParameter("analysis-time-unit");
		assertNotNull(param);
		assertEquals("analysis-time-unit", param.getKey());
		assertEquals("Time unit", param.getName());
		assertNull(param.getValue());
		assertEquals("String", param.getType());
		assertEquals("single", param.getCardinality());
		assertFalse(param.isMandatory());
		assertFalse(param.isManagerParameter());
		
		List<String> possibleValues = param.getPossibleValues();
		assertNotNull(possibleValues);
		assertEquals(4, possibleValues.size());
		assertTrue(possibleValues.contains("s"));
		assertTrue(possibleValues.contains("ms"));
		assertTrue(possibleValues.contains("us"));
		assertTrue(possibleValues.contains("ns"));
	}
}
