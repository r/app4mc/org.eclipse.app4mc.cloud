/*********************************************************************************
 * Copyright (c) 2020, 2021 Robert Bosch GmbH and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */
package org.eclipse.app4mc.cloud.execution;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.Arrays;
import java.util.List;

import org.junit.jupiter.api.Test;

class HeaderHelperTest {

	@Test
	void shouldCheckValidUrls() {
		String baseUrl = "http://localhost:8080/app4mc/converter";
		String toCheckUrl = "http://localhost:8080/app4mc/converter/5240471251956758767";
		
		assertTrue(HeaderHelper.isValid(toCheckUrl, baseUrl));
	}
	
	@Test
	void shouldCheckValidUrlsDifferentProtocols() {
		String baseUrl = "http://localhost:8080/app4mc/converter";
		String toCheckUrl = "https://localhost:8080/app4mc/converter/5240471251956758767";
		
		assertTrue(HeaderHelper.isValid(toCheckUrl, baseUrl));
	}
	
	@Test
	void shouldFireExceptionOnEmptyParameter() {
		String baseUrl = "http://localhost:8080/app4mc/converter";
		String toCheckUrl = "https://localhost:8080/app4mc/converter/5240471251956758767";
		
		assertThrows(ProcessingFailedException.class, () -> {
			HeaderHelper.isValid(null, baseUrl);
		});
		
		assertThrows(ProcessingFailedException.class, () -> {
			HeaderHelper.isValid(toCheckUrl, null);
		});
		
		assertThrows(ProcessingFailedException.class, () -> {
			HeaderHelper.isValid(null, null);
		});
		
		assertThrows(ProcessingFailedException.class, () -> {
			HeaderHelper.isValid("", baseUrl);
		});
		
		assertThrows(ProcessingFailedException.class, () -> {
			HeaderHelper.isValid(toCheckUrl, "");
		});
		
		assertThrows(ProcessingFailedException.class, () -> {
			HeaderHelper.isValid("", "");
		});
	}
	
	@Test
	void shouldFireExceptionOnInvalidUrl() {
		assertThrows(ProcessingFailedException.class, () -> {
			HeaderHelper.isValid("https://localhost:8080/app4mc/converter/5240471251956758767", "localhost:8080/app4mc/converter");
		});
		
		assertThrows(ProcessingFailedException.class, () -> {
			HeaderHelper.isValid("localhost:8080/app4mc/converter/5240471251956758767", "https://localhost:8080/app4mc/converter");
		});
	}
	
	@Test
	void shouldGetResultUrlFromLinkHeader() {
		List<String> linkHeaders = Arrays.asList(
				"<http://localhost:8080/app4mc/converter/5240471251956758767>;rel=\"self\"",
				"<http://localhost:8080/app4mc/converter/5240471251956758767/download>;rel=\"result\"");
		
		String result = HeaderHelper.getUrlFromLink(linkHeaders, "result", "http://localhost:8080/app4mc/converter");
		
		assertEquals("http://localhost:8080/app4mc/converter/5240471251956758767/download", result);
	}
	
	@Test
	void shouldReturnNullForNonExistingRel() {
		List<String> linkHeaders = Arrays.asList(
				"<http://localhost:8080/app4mc/converter/5240471251956758767>;rel=\"self\"",
				"<http://localhost:8080/app4mc/converter/5240471251956758767/download>;rel=\"result\"");
		
		String result = HeaderHelper.getUrlFromLink(linkHeaders, "delete", "http://localhost:8080/app4mc/converter");
		
		assertNull(result);
	}

	@Test
	void shouldReturnMultipleResults() {
		List<String> linkHeaders = Arrays.asList(
				"<http://localhost:8080/app4mc/converter/5240471251956758767/simple>;rel=\"result\"",
				"<http://localhost:8080/app4mc/converter/5240471251956758767/download>;rel=\"result\"");
		
		List<String> urls = HeaderHelper.getUrlsFromLink(linkHeaders, "result", "http://localhost:8080/app4mc/converter");
		
		assertEquals(2, urls.size());
		assertTrue(urls.contains("http://localhost:8080/app4mc/converter/5240471251956758767/simple"));
		assertTrue(urls.contains("http://localhost:8080/app4mc/converter/5240471251956758767/download"));
	}
	
	@Test
	void shouldOnlyReturnValidResult() {
		List<String> linkHeaders = Arrays.asList(
				"<http://localhost:8080/app4mc/converter/5240471251956758767/simple>;rel=\"result\"",
				"<http://www.google.com:8080/app4mc/converter/5240471251956758767/download>;rel=\"result\"");
		
		List<String> urls = HeaderHelper.getUrlsFromLink(linkHeaders, "result", "http://localhost:8080/app4mc/converter");
		
		assertEquals(1, urls.size());
		assertTrue(urls.contains("http://localhost:8080/app4mc/converter/5240471251956758767/simple"));
	}
	
	@Test
	void shouldThrowExceptionForNonEqualHosts() {
		List<String> linkHeaders = Arrays.asList(
				"<http://www.amazon.com/app4mc/converter/5240471251956758767>;rel=\"result\"",
				"<http://www.google.com:8080/app4mc/converter/5240471251956758767/download>;rel=\"result\"");
		
		assertThrows(ProcessingFailedException.class, () -> {
			HeaderHelper.getUrlsFromLink(linkHeaders, "result", "http://localhost:8080/app4mc/converter");
		});
		
	}
}
