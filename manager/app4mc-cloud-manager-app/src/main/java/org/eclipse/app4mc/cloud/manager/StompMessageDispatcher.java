/*********************************************************************************
 * Copyright (c) 2021 Robert Bosch GmbH and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */
package org.eclipse.app4mc.cloud.manager;

import org.eclipse.app4mc.cloud.execution.MessageDispatcher;
import org.springframework.messaging.simp.SimpMessageSendingOperations;

public class StompMessageDispatcher implements MessageDispatcher {

	private final SimpMessageSendingOperations messagingTemplate;
	
	public StompMessageDispatcher(SimpMessageSendingOperations messagingTemplate) {
		this.messagingTemplate = messagingTemplate;
	}

	@Override
	public void send(String destination, Object payload) {
		this.messagingTemplate.convertAndSend(destination, payload);
	}

}
