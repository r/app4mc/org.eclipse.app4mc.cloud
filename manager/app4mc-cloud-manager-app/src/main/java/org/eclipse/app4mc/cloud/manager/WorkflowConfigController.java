/*********************************************************************************
 * Copyright (c) 2020 Robert Bosch GmbH and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */
package org.eclipse.app4mc.cloud.manager;

import java.io.IOException;
import java.text.DateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import javax.annotation.PreDestroy;
import javax.annotation.Resource;

import org.eclipse.app4mc.cloud.execution.CloudServiceDefinition;
import org.eclipse.app4mc.cloud.execution.CloudServiceExecutionRunnable;
import org.eclipse.app4mc.cloud.execution.WorkflowStatus;
import org.eclipse.app4mc.cloud.execution.WorkflowStatusHelper;
import org.eclipse.app4mc.cloud.manager.administration.WorkflowStatusMap;
import org.eclipse.app4mc.cloud.manager.storage.SpringFileSystemStorageService;
import org.eclipse.app4mc.cloud.storage.StorageFileNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.messaging.simp.SimpMessageSendingOperations;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@Controller
public class WorkflowConfigController {

	private ExecutorService executor = Executors.newCachedThreadPool();
	
	private DateFormat formatter = DateFormat.getDateTimeInstance(
            DateFormat.SHORT, 
            DateFormat.SHORT, 
            Locale.ENGLISH);
	
	@Resource(name = "workflowStatusMap")
	private WorkflowStatusMap workflowStatusMap;

	@Resource(name = "cloudServiceDefinitions")
	List<CloudServiceDefinition> cloudServiceDefinitions;
	
	@Resource(name = "exampleModelFiles")
	List<String> exampleModelFiles;
	
	@Resource(name = "exampleConfigFiles")
	List<String> exampleConfigFiles;

	@Autowired
	private SpringFileSystemStorageService storageService;

	@Autowired
    private SimpMessageSendingOperations messagingTemplate;
	
	@Value("classpath:static/examples/*")
    private org.springframework.core.io.Resource[] exampleResources;
	
	@Value("classpath:static/example_configurations/*")
	private org.springframework.core.io.Resource[] exampleConfigResources;

	@GetMapping("/workflowConfig")
	public String workflowConfig() {
		return "workflowConfig";
	}
	
	@PostMapping("/workflowConfig")
	public String handleFileUpload(
			@RequestParam(name = "file", required = false) MultipartFile file, 
			@RequestParam(name = "example", required = false) String example, 
			@RequestParam("configFile") MultipartFile configFile,
			@RequestParam(name = "example_config", required = false) String example_config, 
			RedirectAttributes redirectAttributes) {
		
		if (((file == null || file.isEmpty()) && StringUtils.isEmpty(example))) {
			redirectAttributes.addFlashAttribute(
					"message",
					"You need to select an input file or an example model!");

			return "redirect:/workflowConfig";
		}
		
		if (((configFile == null || configFile.isEmpty()) && StringUtils.isEmpty(example_config))) {
			redirectAttributes.addFlashAttribute(
					"message",
					"You need to select a config file or an example configuration!");
			
			return "redirect:/workflowConfig";
		}
		
		String uuid = null;
		String filename = null;
		String message = null;
		if (file != null && !file.isEmpty()) {
			// upload the input file
			try {
				uuid = this.storageService.store(file.getInputStream(), file.getOriginalFilename());
			} catch (IOException e) {
				redirectAttributes.addFlashAttribute(
						"message",
						"Failed to upload the input file: " + e.getMessage());
				
				return "redirect:/workflowConfig";
			}
			filename = file.getOriginalFilename();
			message = filename + " successfully uploaded";
		} else {
			// copy the example file
			uuid = this.storageService.copyExample(example, exampleResources);
			filename = example;
			message = filename + " example used";
		}
		
		WorkflowStatus workflowStatus = null;
		if (configFile != null && !configFile.isEmpty()) {
			// upload the config file
			try {
				this.storageService.store(uuid, configFile.getInputStream(), configFile.getOriginalFilename());
			} catch (IOException e) {
				redirectAttributes.addFlashAttribute(
						"message",
						"Failed to upload the config file: " + e.getMessage());
				
				return "redirect:/workflowConfig";
			}
			
			// load the workflow status from config file
			workflowStatus = WorkflowStatusHelper.loadWorkflowStatus(
					this.storageService.load(uuid, configFile.getOriginalFilename()).toFile(), 
					this.cloudServiceDefinitions);
		} else {
			// copy the example configuration file
			this.storageService.copyExample(uuid, example_config, exampleConfigResources);

			// load the workflow status from config file
			workflowStatus = WorkflowStatusHelper.loadWorkflowStatus(
					this.storageService.load(uuid, example_config).toFile(), 
					this.cloudServiceDefinitions);
		}
		
		if (workflowStatus == null) {
			redirectAttributes.addFlashAttribute(
					"message",
					"Failed to load the configuration! Ensure the configuration is a valid workflow JSON file!");

			return "redirect:/workflowConfig";
		}
		
		// add default service configurations if not specified
		WorkflowStatusHelper.addDefaultConfigurations(workflowStatus);
		
		workflowStatus.setUuid(uuid);
		if (StringUtils.isEmpty(workflowStatus.getName())) {
			// set a default name in case none was provided
			workflowStatus.setName(uuid + " - " + filename + " - " + this.formatter.format(new Date()));
		} else {
			// prefix the name with the uuid and suffix it with the date to avoid duplicate key errors
			workflowStatus.setName(uuid + " - " + workflowStatus.getName() + " - " + this.formatter.format(new Date()));
		}
		workflowStatus.addMessage(message);
		workflowStatus.addMessage(configFile.getOriginalFilename() + " successfully uploaded");

		this.workflowStatusMap.put(uuid, workflowStatus);
		
		// start processing
		if (workflowStatus.getSelectedServices() != null) {
			CloudServiceExecutionRunnable runnable = new CloudServiceExecutionRunnable(
					this.storageService,
					new StompMessageDispatcher(this.messagingTemplate),
					uuid, 
					filename, 
					workflowStatus);
			this.executor.execute(runnable);
		}
		
		// redirect to workflow with uuid
		return "redirect:/workflow?uuid=" + uuid;
	}
	
	@ModelAttribute("exampleModelFiles")
	public List<String> exampleModelFiles() {
		return this.exampleModelFiles;
	}
	
	@ModelAttribute("exampleConfigFiles")
	public List<String> exampleConfigFiles() {
		return this.exampleConfigFiles;
	}

	@ExceptionHandler(StorageFileNotFoundException.class)
	public ResponseEntity<?> handleStorageFileNotFound(StorageFileNotFoundException exc) {
		return ResponseEntity.notFound().build();
	}

	@PreDestroy
	public void dispose() {
		this.executor.shutdownNow();
	}
	
}