/*********************************************************************************
 * Copyright (c) 2020 Robert Bosch GmbH and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */
package org.eclipse.app4mc.cloud.manager;

import java.io.IOException;
import java.net.URISyntaxException;
import java.text.DateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import javax.annotation.PreDestroy;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.eclipse.app4mc.cloud.execution.CloudServiceDefinition;
import org.eclipse.app4mc.cloud.execution.CloudServiceExecutionRunnable;
import org.eclipse.app4mc.cloud.execution.WorkflowStatus;
import org.eclipse.app4mc.cloud.execution.WorkflowStatusHelper;
import org.eclipse.app4mc.cloud.manager.administration.WorkflowStatusMap;
import org.eclipse.app4mc.cloud.manager.storage.SpringFileSystemStorageService;
import org.eclipse.app4mc.cloud.storage.StorageFileNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.server.mvc.WebMvcLinkBuilder;
import org.springframework.http.CacheControl;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.messaging.simp.SimpMessageSendingOperations;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.util.UriComponents;

@RestController
@RequestMapping("/rest")
public class WorkflowRestController {

	private ExecutorService executor = Executors.newCachedThreadPool();
	
	private DateFormat formatter = DateFormat.getDateTimeInstance(
            DateFormat.SHORT, 
            DateFormat.SHORT, 
            Locale.ENGLISH);
	
	@Resource(name = "workflowStatusMap")
	private WorkflowStatusMap workflowStatusMap;

	@Resource(name = "cloudServiceDefinitions")
	List<CloudServiceDefinition> cloudServiceDefinitions;

	@Autowired
	private SpringFileSystemStorageService storageService;

	@Autowired
    private SimpMessageSendingOperations messagingTemplate;
	
	@PostMapping(value = "/startWorkflow", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
	public ResponseEntity<?> startWorkflow(
			@RequestParam("file") MultipartFile file, 
			@RequestParam("configFile") MultipartFile configFile,
			HttpServletRequest request) throws URISyntaxException {
		
		if (file.isEmpty() || configFile.isEmpty()) {
			return ResponseEntity
					.badRequest()
					.body("No input model file and/or no config file provided as upload");
		}

		// upload the input file
		String uuid;
		try {
			uuid = this.storageService.store(file.getInputStream(), file.getOriginalFilename());
		} catch (IOException e) {
			return ResponseEntity
					.badRequest()
					.body("Failed to upload the input file: " + e.getMessage());
		}
		String filename = file.getOriginalFilename();
		String message = filename + " successfully uploaded";
		
		// upload the config file
		try {
			this.storageService.store(uuid, configFile.getInputStream(), file.getOriginalFilename());
		} catch (IOException e) {
			return ResponseEntity
					.badRequest()
					.body("Failed to upload the config file: " + e.getMessage());
		}
		
		// load the workflow status from config file
		WorkflowStatus workflowStatus = WorkflowStatusHelper.loadWorkflowStatus(
				this.storageService.load(uuid, configFile.getOriginalFilename()).toFile(), 
				this.cloudServiceDefinitions);
		
		// add default service configurations if not specified
		WorkflowStatusHelper.addDefaultConfigurations(workflowStatus);
		
		if (workflowStatus == null) {
			return ResponseEntity
					.badRequest()
					.body("Failed to load the configuration! Ensure the configuration is a valid workflow JSON file!");
		}
		
		workflowStatus.setUuid(uuid);
		if (StringUtils.isEmpty(workflowStatus.getName())) {
			// set a default name in case none was provided
			workflowStatus.setName(uuid + " - " + filename + " - " + this.formatter.format(new Date()));
		} else {
			// prefix the name with the uuid and suffix it with the date to avoid duplicate key errors
			workflowStatus.setName(uuid + " - " + workflowStatus.getName() + " - " + this.formatter.format(new Date()));
		}
		workflowStatus.addMessage(message);
		workflowStatus.addMessage(configFile.getOriginalFilename() + " successfully uploaded");

		this.workflowStatusMap.put(uuid, workflowStatus);
		
		// start processing
		if (workflowStatus.getSelectedServices() != null) {
			CloudServiceExecutionRunnable runnable = new CloudServiceExecutionRunnable(
					this.storageService,
					new StompMessageDispatcher(this.messagingTemplate),
					uuid, 
					filename, 
					workflowStatus);
			this.executor.execute(runnable);
		}
		
		// Return response
		Link self = WebMvcLinkBuilder
				.linkTo(getClass())
				.withSelfRel();
		Link status = WebMvcLinkBuilder
				.linkTo(WebMvcLinkBuilder.methodOn(this.getClass()).status(uuid))
				.withRel("status");
		
		UriComponents uriComponents = WebMvcLinkBuilder
				.linkTo(WorkflowController.class)
				.slash("workflow")
				.toUriComponentsBuilder()
				.queryParam("uuid", uuid)
				.build();
		
		return ResponseEntity
				.created(status.toUri())
				.header(HttpHeaders.LINK, self.toString(), status.toString())
				.body(uriComponents.toString());

	}
	
	@GetMapping(value = "/{uuid}")
	public ResponseEntity<?> status(@PathVariable String uuid) {
		Link self = WebMvcLinkBuilder
				.linkTo(WebMvcLinkBuilder.methodOn(getClass()).status(uuid))
				.withSelfRel();
		
		WorkflowStatus existing = this.workflowStatusMap.get(uuid);
		if (existing == null) {
			existing = WorkflowStatusHelper.loadWorkflowStatus(
					this.storageService.load(uuid, "workflowstatus.json").toFile(), 
					this.cloudServiceDefinitions);
			if (existing != null) {
				// we could load, so we store it in the local map to avoid loading it again
				this.workflowStatusMap.put(uuid, existing);
			}
		}
		
		if (existing == null) {
			return ResponseEntity.notFound().build();
		}
		
		if (existing.isDone()) {
			Link download = WebMvcLinkBuilder
					.linkTo(WebMvcLinkBuilder.methodOn(getClass()).downloadResult(uuid))
					.withRel("download");

			return ResponseEntity
					.ok()
					.header(
							HttpHeaders.CACHE_CONTROL, 
							CacheControl.noCache().getHeaderValue(),
							CacheControl.noStore().getHeaderValue())
					.header(HttpHeaders.LINK, self.toString(), download.toString())
					.build();
		} else {
			return ResponseEntity
					.accepted()
					.header(
							HttpHeaders.CACHE_CONTROL, 
							CacheControl.noCache().getHeaderValue(),
							CacheControl.noStore().getHeaderValue())
					.header(HttpHeaders.LINK, self.toString())
					.build();
		}
	}
	
	@GetMapping(value = "/{uuid}/download", produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
	public ResponseEntity<?> downloadResult(@PathVariable String uuid) {
		Link self = WebMvcLinkBuilder
				.linkTo(WebMvcLinkBuilder.methodOn(this.getClass()).downloadResult(uuid))
				.withSelfRel();
		Link delete = WebMvcLinkBuilder
				.linkTo(WebMvcLinkBuilder.methodOn(this.getClass()).delete(uuid))
				.withRel("delete");

		WorkflowStatus existing = this.workflowStatusMap.get(uuid);
		if (existing == null) {
			existing = WorkflowStatusHelper.loadWorkflowStatus(
					this.storageService.load(uuid, "workflowstatus.json").toFile(), 
					this.cloudServiceDefinitions);
			if (existing != null) {
				// we could load, so we store it in the local map to avoid loading it again
				this.workflowStatusMap.put(uuid, existing);
			}
		}
		
		if (existing == null) {
			return ResponseEntity.notFound().build();
		}

		// create archive of results
		try {
			org.springframework.core.io.Resource resultArchive = storageService.getResultArchive(uuid);
			
			return ResponseEntity
					.ok()
					.contentType(MediaType.APPLICATION_OCTET_STREAM)
					.header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"result.zip\"")
					.header(HttpHeaders.LINK, self.toString(), delete.toString())
					.body(resultArchive);
		} catch (Exception e) {
			return ResponseEntity.badRequest().body(e.getMessage());
		}
	}

	@DeleteMapping(value = "/{uuid}")
	public ResponseEntity<?> delete(@PathVariable String uuid) {
		try {
			this.storageService.delete(uuid);
		} catch (StorageFileNotFoundException e) {
			return ResponseEntity.badRequest().body(e.getMessage());
		}
		return ResponseEntity.ok().build();
	}

	@PreDestroy
	public void dispose() {
		this.executor.shutdownNow();
	}
	
}