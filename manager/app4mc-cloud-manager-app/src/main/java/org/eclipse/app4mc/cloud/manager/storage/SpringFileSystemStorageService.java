/*********************************************************************************
 * Copyright (c) 2021 Robert Bosch GmbH and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */
package org.eclipse.app4mc.cloud.manager.storage;

import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Arrays;

import org.eclipse.app4mc.cloud.storage.FileSystemStorageService;
import org.eclipse.app4mc.cloud.storage.StorageException;
import org.eclipse.app4mc.cloud.storage.StorageFileNotFoundException;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.stereotype.Service;

@Service
public class SpringFileSystemStorageService extends FileSystemStorageService {

	public Resource loadAsResource(String uuid, String... path) {
		Path file = load(uuid, path);
		try {
			Resource resource = new UrlResource(file.toUri());
			if (resource.exists() || resource.isReadable()) {
				return resource;
			}
			else {
				throw new StorageFileNotFoundException("Could not read file: " + file);
			}
		} catch (MalformedURLException e) {
			throw new StorageFileNotFoundException("Could not read file: " + file, e);
		}
	}

	public String copyExample(String filename, Resource[] exampleResources) {
		try {
			Path path = Files.createTempDirectory(TEMP_DIR_PREFIX);
			
			// extract uuid from pathname
			String uuid = path.toString().substring(path.toString().lastIndexOf('_') + 1);
			
			copyExample(path, filename, exampleResources);
			
			return uuid;
		} catch (IOException e) {
			throw new StorageException("Failed to copy example file " + filename, e);
		}
	}
	
	public void copyExample(String uuid, String filename, Resource[] exampleResources) {
		Path path = this.tempLocation.resolve(TEMP_DIR_PREFIX + uuid);
		copyExample(path, filename, exampleResources);
	}
	
	private void copyExample(Path path, String filename, Resource[] exampleResources) {
		try {
			Resource resource = Arrays.stream(exampleResources)
					.filter(res -> filename.equals(res.getFilename()))
					.findFirst()
					.orElse(null);
			if (resource != null) {
				Files.copy(resource.getInputStream(), path.resolve(filename));
			} else {
				throw new StorageException("Could not access example file " + filename);
			}
		} catch (IOException e) {
			throw new StorageException("Failed to copy example file " + filename, e);
		}
	}
	
	public Resource getResultArchive(String uuid) throws IOException {
		Path folder = load(uuid);
		zipResult(folder);
		return loadAsResource(uuid, "result.zip");
	}

}
