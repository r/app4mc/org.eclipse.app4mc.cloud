/*********************************************************************************
 * Copyright (c) 2020, 2021 Robert Bosch GmbH and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */
package org.eclipse.app4mc.cloud.manager;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.util.StringUtils;

import kong.unirest.Unirest;

@SpringBootApplication(scanBasePackages = "org.eclipse.app4mc.cloud")
public class App4McCloudManagerApplication {

	public static void main(String[] args) {
        String proxyHost = System.getProperty("proxy.host");
        String proxyPort = System.getProperty("proxy.port");
        String proxyUser = System.getProperty("proxy.user");
        String proxyPwd = System.getProperty("proxy.pwd");
        
        if (!StringUtils.isEmpty(proxyHost) && !StringUtils.isEmpty(proxyPort)) {
        	if (!StringUtils.isEmpty(proxyUser) && !StringUtils.isEmpty(proxyPwd)) {
        		Unirest.config()
        			.socketTimeout(0)
        			.proxy(proxyHost, Integer.valueOf(proxyPort), proxyUser, proxyPwd);
        	} else {
        		Unirest.config()
        			.socketTimeout(0)
        			.proxy(proxyHost, Integer.valueOf(proxyPort));
        	}
        }

		SpringApplication.run(App4McCloudManagerApplication.class, args);
	}
}
