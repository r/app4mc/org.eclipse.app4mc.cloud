/*********************************************************************************
 * Copyright (c) 2020, 2021 Robert Bosch GmbH and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 *     Dortmund University of Applied Sciences and Arts
 ********************************************************************************
 */
package org.eclipse.app4mc.cloud.manager;

import java.io.IOException;
import java.text.DateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import javax.annotation.PreDestroy;
import javax.servlet.http.HttpServletRequest;

import org.eclipse.app4mc.cloud.execution.CloudServiceDefinition;
import org.eclipse.app4mc.cloud.execution.CloudServiceExecutionRunnable;
import org.eclipse.app4mc.cloud.execution.ProcessLog;
import org.eclipse.app4mc.cloud.execution.ProcessLog.Action;
import org.eclipse.app4mc.cloud.execution.ServiceConfiguration;
import org.eclipse.app4mc.cloud.execution.ServiceConfigurationParameter;
import org.eclipse.app4mc.cloud.execution.ServiceNode;
import org.eclipse.app4mc.cloud.execution.WorkflowStatus;
import org.eclipse.app4mc.cloud.execution.WorkflowStatusHelper;
import org.eclipse.app4mc.cloud.manager.administration.WorkflowStatusMap;
import org.eclipse.app4mc.cloud.manager.storage.SpringFileSystemStorageService;
import org.eclipse.app4mc.cloud.storage.StorageFileNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.simp.SimpMessageSendingOperations;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.multipart.MultipartFile;

@Controller
@SessionAttributes("workflowStatus")
public class WorkflowController {

	private final SpringFileSystemStorageService storageService;

	private ExecutorService executor = Executors.newCachedThreadPool();
	
	private DateFormat formatter = DateFormat.getDateTimeInstance(
            DateFormat.SHORT, 
            DateFormat.SHORT, 
            Locale.ENGLISH);
	
	@Value("classpath:static/examples/*")
    private Resource[] exampleResources;

	@javax.annotation.Resource(name = "workflowStatusMap")
	private WorkflowStatusMap workflowStatusMap;
	
	@javax.annotation.Resource(name = "cloudServiceDefinitions")
	List<CloudServiceDefinition> cloudServiceDefinitions;
	
	@javax.annotation.Resource(name = "exampleModelFiles")
	List<String> exampleModelFiles;

	@Autowired
    public SimpMessageSendingOperations messagingTemplate;
	
	@Autowired
	public WorkflowController(SpringFileSystemStorageService storageService) {
		this.storageService = storageService;
	}

	@GetMapping("/workflow")
	public String workflow(
			Model model, 
			@ModelAttribute WorkflowStatus workflowStatus,
			@RequestParam(name = "uuid", required = false) String uuid) {
		
		if (!StringUtils.isEmpty(uuid)) {
			
			WorkflowStatus existing = this.workflowStatusMap.get(uuid);
			if (existing == null) {
				existing = WorkflowStatusHelper.loadWorkflowStatus(
						this.storageService.load(uuid, "workflowstatus.json").toFile(), 
						this.cloudServiceDefinitions);
				if (existing != null) {
					// we could load, so we store it in the local map to avoid loading it again
					this.workflowStatusMap.put(uuid, existing);
				} else {
					existing = new WorkflowStatus();
				}
			}
			model.addAttribute("workflowStatus", existing);
		} else if (workflowStatus == null || workflowStatus.isDone()) {
			// only create a new WorkflowStatus in case the current instance is done
			model.addAttribute("workflowStatus", new WorkflowStatus());
		}
		
		// render the form view
		return "workflow";
	}
	
	@PostMapping("/workflow")
	public String handleFileUpload(
			@RequestParam(name = "file", required = false) MultipartFile file, 
			@RequestParam(name = "example", required = false) String example, 
			Model model, 
			@ModelAttribute WorkflowStatus workflowStatus) {
		
		if ((file == null || file.isEmpty()) && StringUtils.isEmpty(example)) {
			workflowStatus.addError("Select a file to upload or an example");
			return "redirect:/workflow";
		}
		
		if (workflowStatus.getUuid() != null) {
			// we need to ensure that a new WorkflowStatus is created
			workflowStatus = new WorkflowStatus();
			model.addAttribute("workflowStatus", workflowStatus);
		}
		
		String uuid = null;
		String filename = null;
		String message = null;
		if (file != null && !file.isEmpty()) {
			// upload the input file
			try {
				uuid = this.storageService.store(file.getInputStream(), file.getOriginalFilename());
			} catch (IOException e) {
				workflowStatus.addError("Failed to upload the input file: " + e.getMessage());
				return "redirect:/workflow";
			}
			filename = file.getOriginalFilename();
			message = filename + " successfully uploaded";
		} else {
			// copy the example file
			uuid = this.storageService.copyExample(example, exampleResources);
			filename = example;
			message = filename + " example used";
		}
		
		workflowStatus.setUuid(uuid);
		workflowStatus.setName(uuid + " - " + filename + " - " + formatter.format(new Date()));
		workflowStatus.addMessage(message);
		
		this.messagingTemplate.convertAndSend(
				"/topic/process-updates/" + uuid, 
				new ProcessLog(Action.UPDATE, message));

		this.workflowStatusMap.put(uuid, workflowStatus);
		
		if (workflowStatus.getSelectedServices() != null) {
			CloudServiceExecutionRunnable runnable = new CloudServiceExecutionRunnable(
					storageService,
					new StompMessageDispatcher(this.messagingTemplate),
					uuid, 
					filename, 
					workflowStatus);
			this.executor.execute(runnable);
		}
		
		return "redirect:/workflow?uuid=" + uuid;
	}

	@PostMapping("/clear")
	public String clearWorkflow(Model model) {
		model.addAttribute("workflowStatus", new WorkflowStatus());
		return "workflow";
	}
	
	@PostMapping("/cancel")
	public String cancelWorkflow(@ModelAttribute WorkflowStatus workflowStatus) {
		if (workflowStatus != null) {
			workflowStatus.cancel();
		}
		return "workflow";
	}
	
	@PostMapping("/profile/{selected}")
	public String selectServiceProfile(
			@PathVariable(name = "selected") String selected,
			@ModelAttribute WorkflowStatus ws) {
		
		if ("systemc".equals(selected)) {
			selectService("migration", ws);
			selectService("validation", ws);
			selectService("amlt2systemc", ws);
			selectService("app4mc_sim", ws);
			selectService("inchron_btf_visualization", ws);
			
			// enable "APP4MC.sim Validations"
			ServiceConfiguration configuration = ws.getConfiguration("validation");
			ServiceConfigurationParameter parameter = configuration.getParameter("profiles");
			if (parameter != null) {
				parameter.setValue("APP4MC.sim Validations");
			}
			
			// add default configuration to avoid timeout in simulation
			configuration = ws.getConfiguration("app4mc_sim");
			parameter = configuration.getParameter("timeout");
			parameter.setValue("-1");
		} else if ("rtc".equals(selected)) {
			selectService("migration", ws);
			selectService("validation", ws);
			selectService("rtc_analysis", ws);
			selectService("rtc_interpreter", ws);
			selectService("chart_visualizer", ws);
			
			// add default configuration to ignore over utilization
			ServiceConfiguration configuration = ws.getConfiguration("rtc_analysis");
			ServiceConfigurationParameter parameter = configuration.getParameter("analysis-ignore-overutilization");
			if (parameter != null) {
				parameter.setValue("true");
			}
		}
		
		return "workflow";
	}
	
	@PostMapping("/select/{selected}")
	public String selectService(
			@PathVariable(name = "selected") String selected,
			@ModelAttribute WorkflowStatus ws) {

		CloudServiceDefinition csd = this.cloudServiceDefinitions.stream()
				.filter(sd -> sd.getKey().equals(selected))
				.findFirst()
				.orElse(null);
		
		if (csd == null) {
			return "workflow";
		}

		ws.addSelectedService(csd, WorkflowStatusHelper.getConfigurationForService(csd));
		
		// render the form view
		return "workflow";
	}
	
	@PostMapping("/select/{parentId}/{selected}")
	public String selectService(
			@PathVariable(name = "selected") String selected,
			@PathVariable(name = "parentId") String parentId,
			@ModelAttribute WorkflowStatus ws) {

		CloudServiceDefinition csd = this.cloudServiceDefinitions.stream()
				.filter(sd -> sd.getKey().equals(selected))
				.findFirst()
				.orElse(null);
		
		if (csd == null) {
			return "workflow";
		}

		ws.addSelectedService(parentId, csd, WorkflowStatusHelper.getConfigurationForService(csd));
		
		// render the form view
		return "workflow";
	}
	
	@PostMapping("/remove/{selected}")
	public String removeService(
			@PathVariable(name = "selected") String selected,
			@ModelAttribute WorkflowStatus ws) {
		
		ws.removeSelectedService(selected);
		
		// render the form view
		return "workflow";
	}
	
	@PostMapping("/remove/{parentId}/{selected}")
	public String removeService(
			@PathVariable(name = "parentId") String parentId,
			@PathVariable(name = "selected") String selected,
			@ModelAttribute WorkflowStatus ws) {
		
		ws.removeSelectedService(parentId, selected);

		return "workflow";
	}
	
	@PostMapping("/removeall")
	public String removeAllServices(@ModelAttribute WorkflowStatus ws) {
		ws.clear();
		// render the form view
		return "workflow";
	}
	
	@GetMapping("/selectedServices")
	public String getSelectedServices() {
		// render the servicesList fragment contained in selectedServices.html
		return "selectedServices :: servicesList";
	}
	
	@GetMapping("/messages")
	public String getWorkflowMessages() {
		// render the messagesList fragment contained in status.html
		return "status :: messagesList";
	}
	
	@GetMapping("/errors")
	public String getWorkflowErrors() {
		// render the errorList fragment contained in status.html
		return "status :: errorList";
	}
	
	@GetMapping("/results")
	public String getWorkflowResults() {
		// render the resultList fragment contained in status.html
		return "status :: resultList";
	}
	
	@GetMapping("/config/{id}")
	public String getConfiguration(@PathVariable("id") String qualifiedId, Model model, @ModelAttribute WorkflowStatus ws) {
		ServiceNode node = ws.getServiceByQualifiedId(qualifiedId);
		model.addAttribute("name", node.getService().getName());
		model.addAttribute("id", node.getQualifiedId());
		model.addAttribute("config", node.getServiceConfiguration());
		model.addAttribute("uuid", ws.getUuid());

		// render the configuration
		return "configuration :: config";
	}
	
	@PostMapping("/config/{id}")
	public String saveConfiguration(@PathVariable("id") String qualifiedId, @ModelAttribute ServiceConfiguration config, 
			Model model, @ModelAttribute WorkflowStatus ws) {
		
		// render the configuration
		ServiceNode node = ws.getServiceByQualifiedId(qualifiedId);
		ServiceConfiguration nodeConfig = node.getServiceConfiguration();
		nodeConfig.getParameterList().clear();
		nodeConfig.getParameterList().addAll(config.getParameterList());
		
		model.addAttribute("name", node.getService().getName());
		model.addAttribute("id", qualifiedId);
		model.addAttribute("config", node.getServiceConfiguration());
		
		return "configuration :: config";
	}
	
	@PutMapping("/config/{id}")
	public String resetConfiguration(@PathVariable("id") String id, Model model, @ModelAttribute WorkflowStatus ws) {
		
		ServiceNode node = ws.getServiceByQualifiedId(id);
		
		CloudServiceDefinition csd = this.cloudServiceDefinitions.stream()
				.filter(sd -> sd.getKey().equals(node.getService().getKey()))
				.findFirst()
				.orElse(null);
		
		ServiceConfiguration defaultConfig = WorkflowStatusHelper.getConfigurationForService(csd);
				
		model.addAttribute("name", node.getService().getName());
		model.addAttribute("id", id);
		model.addAttribute("config", defaultConfig);
		
		return "configuration :: config";
	}

	@GetMapping("/{uuid}/files/**")
	@ResponseBody
	public ResponseEntity<Resource> serveFile(
			@PathVariable String uuid,
			HttpServletRequest request,
			@RequestParam(name = "download", required = false) String download) {

		String servletPath = request.getServletPath();
		String filePath = servletPath.substring(("/"+uuid+"/files/").length());
		
		Resource file = this.storageService.loadAsResource(uuid, filePath.split("/"));
		String filename = file.getFilename();
		
		if (!StringUtils.isEmpty(download) && "true".equals(download)) {
			return ResponseEntity.ok().header(HttpHeaders.CONTENT_DISPOSITION,
					"attachment; filename=\"" + file.getFilename() + "\"").body(file);
		}
		
		if (filename.endsWith("html")) {
			return ResponseEntity.ok().contentType(MediaType.TEXT_HTML).body(file);
		} else if (filename.endsWith("pdf")) {
			return ResponseEntity.ok().contentType(MediaType.APPLICATION_PDF).body(file);
		} else if (filename.endsWith("gif")) {
			return ResponseEntity.ok().contentType(MediaType.IMAGE_GIF).body(file);
		} else if (filename.endsWith("jpg") || filename.endsWith("jpeg")) {
			return ResponseEntity.ok().contentType(MediaType.IMAGE_JPEG).body(file);
		} else if (filename.endsWith("png")) {
			return ResponseEntity.ok().contentType(MediaType.IMAGE_PNG).body(file);
		}
		return ResponseEntity.ok().contentType(MediaType.TEXT_PLAIN).body(file);
	}

	@GetMapping("/{uuid}/delete")
	public String delete(
			@PathVariable String uuid,
			Model model) {
		
		this.storageService.delete(uuid);
		model.addAttribute("workflowStatus", new WorkflowStatus());

		return "redirect:/workflow";
	}
	
	@GetMapping(value = "/{uuid}/configdownload", produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
	public ResponseEntity<?> downloadConfiguration(@PathVariable String uuid) {

		WorkflowStatus existing = this.workflowStatusMap.get(uuid);
		if (existing == null) {
			existing = WorkflowStatusHelper.loadWorkflowStatus(
					this.storageService.load(uuid, "workflowstatus.json").toFile(), 
					this.cloudServiceDefinitions);
			if (existing != null) {
				// we could load, so we store it in the local map to avoid loading it again
				this.workflowStatusMap.put(uuid, existing);
			}
		}
		
		if (existing == null) {
			return ResponseEntity.notFound().build();
		}

		// create workflow configuration json
		try {
			WorkflowStatus config = new WorkflowStatus();
			config.addAllSelectedServices(existing.getSelectedServices());
			
			return ResponseEntity
					.ok()
					.contentType(MediaType.APPLICATION_OCTET_STREAM)
					.header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"workflowConfig.json\"")
					.body(WorkflowStatusHelper.getWorkflowStatusAsJSON(config));
		} catch (Exception e) {
			return ResponseEntity.badRequest().body(e.getMessage());
		}
	}
	
	@ExceptionHandler(StorageFileNotFoundException.class)
	public ResponseEntity<?> handleStorageFileNotFound(StorageFileNotFoundException exc) {
		return ResponseEntity.notFound().build();
	}

	@ModelAttribute("workflowStatus")
	public WorkflowStatus workflowStatus() {
		return new WorkflowStatus();
	}

	@ModelAttribute("cloudServiceDefinitions")
	public List<CloudServiceDefinition> cloudServiceDefinitions() {
		return this.cloudServiceDefinitions;
	}
	
	@ModelAttribute("exampleModelFiles")
	public List<String> exampleModelFiles() {
		return this.exampleModelFiles;
	}

	@PreDestroy
	public void dispose() {
		this.executor.shutdownNow();
	}
	
	// the register/unregister is added to ensure that the DONE message is sent only
	// if a websocket connection is established to ensure that the final refresh is
	// performed automatically
	
	@MessageMapping("/register")
	public void registerConnection(String uuid) {
		WorkflowStatus existing = this.workflowStatusMap.get(uuid);
		if (existing != null) {
			existing.connect();
		}
	}
	
	@MessageMapping("/unregister")
	public void unregisterConnection(String uuid) {
		WorkflowStatus existing = this.workflowStatusMap.get(uuid);
		if (existing != null) {
			existing.disconnect();
		}
	}
}