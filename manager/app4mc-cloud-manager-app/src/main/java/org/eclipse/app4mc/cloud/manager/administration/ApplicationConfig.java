/*********************************************************************************
 * Copyright (c) 2020, 2021 Robert Bosch GmbH and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */
package org.eclipse.app4mc.cloud.manager.administration;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import org.eclipse.app4mc.cloud.execution.CloudServiceDefinition;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.Resource;
import org.springframework.util.ResourceUtils;
import org.springframework.web.context.annotation.ApplicationScope;

@Configuration
public class ApplicationConfig {

	@Value("classpath:static/examples/*")
    private Resource[] exampleResources;
	
	@Value("classpath:static/example_configurations/*")
	private Resource[] exampleConfigResources;
	
	@Bean
    @ApplicationScope
    public WorkflowStatusMap workflowStatusMap() {
		return new WorkflowStatusMap();
	}

	@Bean
    @ApplicationScope
    public List<CloudServiceDefinition> cloudServiceDefinitions() {
		ArrayList<CloudServiceDefinition> definitions = new ArrayList<>();

		String config = System.getProperty("service.configuration");
		if (config != null) {
			try {
				File file = ResourceUtils.getFile(config);
				Path path = Paths.get(file.toURI());
				List<CloudServiceDefinition> services = Files.lines(path).map(line -> {
					String[] split = line.split(";");
					String key = split[0];
					String name = split[1];
					String url = split[2];
					String desc = split.length >= 4 ? split[3] : "";
					boolean configAvailable = split.length >= 5 ? Boolean.getBoolean(split[4]) : true;
					return new CloudServiceDefinition(key, name, url, desc, configAvailable);
				}).collect(Collectors.toList());
				definitions.addAll(services);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
		if (definitions.isEmpty()) {
			definitions.add(new CloudServiceDefinition("migration", "Migration", "http://localhost:8080/app4mc/migration/", "Model Migration Service"));
			definitions.add(new CloudServiceDefinition("validation", "Validation", "http://localhost:8181/app4mc/validation/", "Model Validation Service"));
		}
        
        return definitions;
    }

	@Bean
    @ApplicationScope
    public List<String> exampleModelFiles() {
		List<String> examples = new ArrayList<>();
		
		if (exampleResources != null) {
			examples = Arrays.stream(exampleResources).map(Resource::getFilename).collect(Collectors.toList());
		}
		
		Collections.sort(examples, String.CASE_INSENSITIVE_ORDER);
		
		return examples;
	}
	
	@Bean
	@ApplicationScope
	public List<String> exampleConfigFiles() {
		List<String> examples = new ArrayList<>();
		
		if (exampleConfigResources != null) {
			examples = Arrays.stream(exampleConfigResources).map(Resource::getFilename).collect(Collectors.toList());
		}
		
		Collections.sort(examples, String.CASE_INSENSITIVE_ORDER);
		
		return examples;
	}
}
