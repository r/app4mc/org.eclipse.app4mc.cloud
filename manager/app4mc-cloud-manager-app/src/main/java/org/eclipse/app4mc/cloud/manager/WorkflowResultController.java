/*********************************************************************************
 * Copyright (c) 2020, 2021 Robert Bosch GmbH and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */
package org.eclipse.app4mc.cloud.manager;

import java.util.List;
import java.util.stream.Collectors;

import javax.annotation.Resource;

import org.eclipse.app4mc.cloud.execution.CloudServiceDefinition;
import org.eclipse.app4mc.cloud.execution.WorkflowStatus;
import org.eclipse.app4mc.cloud.execution.WorkflowStatusHelper;
import org.eclipse.app4mc.cloud.manager.administration.WorkflowStatusMap;
import org.eclipse.app4mc.cloud.storage.StorageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.mvc.method.annotation.MvcUriComponentsBuilder;

@Controller
public class WorkflowResultController {

	@Autowired
	private StorageService storageService;
	
	@Resource(name = "workflowStatusMap")
	private WorkflowStatusMap workflowStatusMap;

	@Resource(name = "cloudServiceDefinitions")
	List<CloudServiceDefinition> cloudServiceDefinitions;

	@GetMapping("/workflowResults")
	public String workflowResults(Model model) {
		
		model.addAttribute("workflows", storageService.loadAll()
				.map(path -> path.toString().substring(path.toString().lastIndexOf('_') + 1))
				.collect(Collectors.toMap(
						uuid -> {
							WorkflowStatus status = WorkflowStatusHelper.loadWorkflowStatus(
									this.storageService.load(uuid, "workflowstatus.json").toFile(),
									this.cloudServiceDefinitions,
									false);
							return (status != null && status.getName() != null) ? status.getName() : uuid;
						}, 
						uuid -> MvcUriComponentsBuilder.fromMethodName(WorkflowController.class,
							"workflow",
							model,
							null,
							uuid).build().toUri().toString()))
		);

		// render the form view
		return "workflowResults";
	}

	@GetMapping("/deleteAllResults")
	public String delete(Model model) {
		
		storageService.deleteAll();
		// remove possible status from model
		model.addAttribute("workflowStatus", new WorkflowStatus());
		// clear stored WorkflowStatus
		this.workflowStatusMap.clear();

		return "redirect:/workflowResults";
	}

}