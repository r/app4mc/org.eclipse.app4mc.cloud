/*********************************************************************************
 * Copyright (c) 2020 Robert Bosch GmbH and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */
package org.eclipse.app4mc.cloud.manager.administration;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.app4mc.cloud.execution.CloudServiceDefinition;

public class CloudServiceDefinitionDTO {

	private ArrayList<CloudServiceDefinition> services = new ArrayList<>();
	
	public CloudServiceDefinitionDTO(List<CloudServiceDefinition> services) {
		if (services != null) {
			this.services = new ArrayList<CloudServiceDefinition>(services);
		} else {
			this.services = new ArrayList<>();
		}
	}

	public ArrayList<CloudServiceDefinition> getServices() {
		return services;
	}

	public void setServices(ArrayList<CloudServiceDefinition> services) {
		this.services = services;
	}

	public void addService(CloudServiceDefinition service) {
		this.services.add(service);
	}
}
