function removeService(service) {
	$.ajax({
		type: 'POST',
		url: '/admin/remove/' + service,
		success: function(result) {
			location.reload();
		}
	});
}