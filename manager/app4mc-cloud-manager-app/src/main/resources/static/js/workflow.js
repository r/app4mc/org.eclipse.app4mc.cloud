function clearWorkflow() {
	$.ajax({
		type: 'POST',
		url: '/clear',
		success: function(result) {
			loadFragments();
		}
	});
}

function cancelWorkflow() {
	$.ajax({
		type: 'POST',
		url: '/cancel',
		success: function(result) {
			loadFragments();
		}
	});
}

function updateSelectedServices(service) {
	$.ajax({
		type: 'POST',
		url: '/select/' + service,
		success: function(result) {
			$('#selectedServicesBlock').load('/selectedServices');
			$('[data-toggle="tooltip"]').tooltip();
		}
	});
}

function updateSelectedServicesWithParent(service) {
	let parentId = $('#pserviceId').val();
	if (parentId === '--'){
		updateSelectedServices(service);
	} else {
		$.ajax({
			type: 'POST',
			url: '/select/' + parentId + '/' + service,
			success: function(result) {
				$('#selectedServicesBlock').load('/selectedServices');
				$('[data-toggle="tooltip"]').tooltip();
			}
		});
	}
}

function selectServiceProfile(profile) {
	$.ajax({
		type: 'POST',
		url: '/profile/' + profile,
		success: function(result) {
			$('#selectedServicesBlock').load('/selectedServices');
		}
	});
}

function removeSelectedServices(service) {
	$.ajax({
		type: 'POST',
		url: '/remove/' + service,
		success: function(result) {
			$('#selectedServicesBlock').load('/selectedServices');
		}
	});
}

function removeService() {
	let parentId = $('#parentId').val();
	let serviceKey = $('#serviceKey').val();
	
	if (parentId === '--'){
		removeSelectedServices(serviceKey);
		
	}else if (parentId === '' && serviceKey === ''){
		$.ajax({
			type: 'POST',
			url: '/removeall',
			success: function(result) {
				$('#selectedServicesBlock').load('/selectedServices');
			}
		});
		
	}else{
		$.ajax({
			type: 'POST',
			url: '/remove/' + parentId + '/' + serviceKey,
			success: function(result) {
				$('#selectedServicesBlock').load('/selectedServices');
			}
		});
	}
}

function toggleBranch(element){
    element.parentElement.querySelector(".nested").classList.toggle("active");
    element.classList.toggle("caret");
}

function openSelectModal(parentService, parentServiceId){
    $('#serviceModal').css("display", "block");
    $('#pservice').val(parentService);
    $('#pserviceId').val(parentServiceId);
}

function openRemoveAllServiceModal() {
    $('#removeserviceModal').css("display", "block");
    const header = "Delete All Services";
    const text = "Are you sure you want to delete all services?";

    $('#removeServiceHeader').text(header);
    $('#removeServiceTxt').text(text);
    $('#removeModalGrid').css("display", "none");
    $('#parentId').val("")
    $('#serviceKey').val("")
}

function openRemoveServiceModal(parentId, serviceKey) {
    $('#removeserviceModal').css("display", "block");
    const header = "Delete a service";
    const text = "Are you sure you want to delete this service and all its children?";

    $('#removeServiceHeader').text(header);
    $('#removeServiceTxt').text(text);
    $('#removeModalGrid').css("display", "grid");
    $('#parentId').val(parentId)
    $('#serviceKey').val(serviceKey)
}

function openConfigModal(serviceId) {
	$.ajax({
		url: '/config/' + serviceId,
		success: function(result) {
			$("#configModalHolder").html(result);
			$('#configurationModal').css("display", "block");
		}
	});
}

function closeServiceModal(){
    $('#serviceModal').hide();
}

function closeRemoveServiceModal(){
    $('#removeserviceModal').hide();
}

function closeConfigModal(){
    $('#configurationModal').hide();
}

function saveConfiguration(){
    $('#configForm').ajaxSubmit({
    	success: function(result) {
			$('#configurationModal').hide();
		}
    });
}

function resetConfiguration(serviceId){
	$.ajax({
		type: 'PUT',
		url: '/config/' + serviceId,
		success: function(result) {
			$("#configModalHolder").html(result);
			$('#configurationModal').css("display", "block");
		}
	});
}

function displayServiceProfileOptions() {
    $('#dropdown-content').css("display", "block");
    event.stopPropagation();
}

function addConfigurationParameterOption(element) {
	$('#dropdown-content').hide();
    const input = document.getElementById('configurationParameterOptionsSelected');
    if(input.value.includes(element.text)) return;

    if (input.value !== "") {
        input.value = input.value.concat(',', element.text)
    } else {
        input.value = element.text;
    }
    $('#configurationParameterOptionsSelected').trigger("change");
}

function onConfigurationParameterOptionChanged(element) {
    const valArray = element.value.split(',');
    const div = document.getElementById('configurationParameterOptions');
    div.innerHTML = '';
    for (let index = 0; index < valArray.length; index++) {
        if (valArray[index] !== '') {
			const html = 
			'<div class="d-flex bg-info px-2 py-1 mr-1 align-middle tag">\
				<p class="mr-1 my-auto pr-1 border-right border-secondary text-light text-nowrap tagText">' + valArray[index] + '</p>\
				<a class="my-auto text-light tagClose" onclick="removeConfigurationParameterOption(this)">\
					<i class="fas fa-times-circle"></i>\
				</a>\
			</div>';

            div.innerHTML += html;
        }
    }
}

function removeConfigurationParameterOption(element) {
    const value = element.previousElementSibling.innerText;
    const input = document.getElementById('configurationParameterOptionsSelected');
    let valArray = input.value.split(',');
    valArray = valArray.filter(val => val !== value);
    input.value = valArray.join();
    $('#configurationParameterOptionsSelected').trigger("change");
    
    event.stopPropagation()
}

function onConfigurationBackgroundClick() {
	$('#dropdown-content').hide();
}

function loadFragments() {
	$('#selectedServicesBlock').load('/selectedServices');
	$('#messagesBlock').load('/messages');
	$('#errorsBlock').load('/errors');
	$('#resultsBlock').load('/results', function() {
		if (workflowDone) {
			$('html, body').scrollTop($(document).height());
		}
	});
}

$(document).ready(function() {
	// load the fragments to be able to update via ajax in later steps
	loadFragments();
	
	// if the workflow is not done yet and there is a uuid available,
	// connect the websocket and submit to the topic of workflow updates
	if (!workflowDone && uuid) {
		connect();
	} 
	
	// enable all tooltips in the document
	$('[data-toggle="tooltip"]').tooltip();
	
	$('#workflowCancel').on('click', function() {
		$(this).prop('disabled', true);
		cancelWorkflow();
	});
	
	$('#customFile').on('change', function() {
		//get the file name
		var fileName = $(this).val().split('\\').pop();
		//replace the "Choose a file" label
		$(this).next('.custom-file-label').html(fileName);
        $('#profiles').show();
        $('#buttons').show();
        $('#workflowSubmit').show();
		$('#example').val('');
		clearWorkflow();
	});
	
	$('#example').on('change', function() {
        $('#profiles').show();
        $('#buttons').show();
        $('#workflowSubmit').show();
		$('#customFile').val('');
		$('#customFile').next('.custom-file-label').html('');
		clearWorkflow();
	});
});

function copy(link) {
	var copyLink = link.href;
	var handler = function(event) {
		event.clipboardData.setData('text/plain', copyLink);
		event.preventDefault();
		document.removeEventListener('copy', handler, true);
	}
	document.addEventListener('copy', handler, true);
	document.execCommand('copy');
	$('.toast').toast('show');
}

//websocket

var stompClient = null;

function connect() {
	// before we connect new, we need to ensure that a previous connection is disconnected
	disconnect();
	
	var socket = new SockJS('/websocket');
	stompClient = Stomp.over(socket);
	stompClient.connect({}, function (frame) {
		console.log('Connected: ' + frame);
		
		stompClient.subscribe(
			'/topic/process-updates/' + uuid,
	  		function (processLog) {
	  			var processLogObj = JSON.parse(processLog.body);
		     	var action = processLogObj.action;
		     	var message = processLogObj.message;
		     	var processUuid = processLogObj.uuid;
		     	if (action == 'UPDATE') {
		        	$('#messagesBlock').load('/messages');
					$('#errorsBlock').load('/errors');
					
					$('html, body').scrollTop($(document).height());
					
		     	} else if (action == 'DONE') {
		     		disconnect();
		     		window.location.href = window.location.href.split("?")[0] + '?uuid=' + processUuid;
		 	    } else if (action == 'PROCESSING') {
		 	    	showFlash(message);
					
		 	    	$('html, body').scrollTop($(document).height());
		 	    }
			}
		);
		
		stompClient.send('/app/register', {}, uuid);
	});
}

function disconnect() {
	if (stompClient !== null) {
		stompClient.send('/app/unregister', {}, uuid);
		stompClient.disconnect();
		stompClient = null;
		console.log("Disconnected");
	}
}

function showFlash(msg) {
	$('#flashMessage').text(msg);
	$('#flashMessage').slideDown(function() {
	    setTimeout(function() {
	        $('#flashMessage').slideUp();
	    }, 2000);
	});
}
